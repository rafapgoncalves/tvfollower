function TvShow(tvShow) {
	this.id = (tvShow && tvShow.id) ? tvShow.id : undefined;
	this.name = (tvShow && tvShow.name) ? tvShow.name : undefined;
	this.watching = (tvShow && tvShow.watching) ? tvShow.watching : undefined;
	this.ended = (tvShow && tvShow.ended) ? tvShow.ended : undefined;
	this.lastEpisodeWatched = new Episode((tvShow && tvShow.lastEpisodeWatched) ? tvShow.lastEpisodeWatched : null);
	this.nextEpisodeToWatch = new Episode((tvShow && tvShow.nextEpisodeToWatch) ? tvShow.nextEpisodeToWatch : null);
}