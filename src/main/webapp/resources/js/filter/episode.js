(function(){
	var app = angular.module('App');
	
	app.filter('episodeFilter',function(){
		return function(episode) {
			if(episode) {
				return 'S' + (episode.season < 10 ? '0' : '') + episode.season + 'E' + (episode.number < 10 ? '0' : '') + episode.number;
			}
			else {
				return '';
			}
		}
	});
}());