(function(){
	var app = angular.module('App');
	
	app.controller('OrphanedAlertController', ['$scope', function($scope){
		var controller = this;
		
		controller.response = undefined;
		
		controller.clearResponse = function() {
			controller.response = undefined;
		};
		
		$scope.$on('jfxcallback-orphaned', function(event, data) {
			controller.response = data;
			$scope.$apply();
		});
	}]);
	
	app.directive('orphanedAlert', function(){
		return {
			restrict: 'A',
			templateUrl: 'view/orphaned-alert.html',
			controller: 'OrphanedAlertController',
			controllerAs: 'orphanedAlertCtrl'
		};
	});
}());
