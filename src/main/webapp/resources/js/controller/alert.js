(function(){
	var app = angular.module('App');
	
	app.controller('AlertController', ['$scope', function($scope){
		var controller = this;
		
		controller.getStyle = function() {
			if($scope.alertObject != null) {
				if($scope.alertObject.status === 'ERROR') {
					return 'danger';
				}
				else {
					return $scope.alertObject.status.toLowerCase();
				}
			}
		}
		controller.getMessage = function() {
			if($scope.alertObject != null) {
				return $scope.alertObject.message;
			}
		}
		controller.isVisible = function() {
			return ($scope.alertObject != null);
		}
		controller.onClose = $scope.onClose;
	}]);
	
	app.directive('alert', function(){
		return {
			restrict: 'A',
			templateUrl: 'view/alert.html',
			controller: 'AlertController',
			controllerAs: 'alertCtrl',
			scope: {
				alertObject: '=',
				onClose: '&'
			}
		};
	});
}());