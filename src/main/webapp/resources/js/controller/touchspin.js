(function(){
	var app = angular.module('rafax.touchspin', []);
	
	app.controller('TouchSpinController', ['$scope', '$timeout', function($scope, $timeout){
		var controller = this;
		
		controller.updateOptions = function() {
			$('#' + $scope.id).TouchSpin($scope.touchSpinOptions);
		};
		
		$timeout(function(){
			controller.updateOptions();
		}, 0);
		
		$scope.$watch('options', function(){
			controller.updateOptions();
		});
	}]);
	
	app.directive('touchSpin', function(){
		return {
			restrict: 'A',
			template: '<input type="text" id="{{id}}" data-ng-model="ngModel" data-touch-spin-integer />',
			controller: 'TouchSpinController',
			controllerAs: 'ctrl',
			scope: {
				id: '@',
				ngModel: '=',
				touchSpinOptions: '<options'
			}
		};
	});
	
	app.directive('touchSpinInteger', function(){
		return {
			require: 'ngModel',
			link: function(scope, ele, attr, ctrl){
				ctrl.$parsers.unshift(function(viewValue){
					return parseInt(viewValue, 10);
				});
			}
		};
	});
}());