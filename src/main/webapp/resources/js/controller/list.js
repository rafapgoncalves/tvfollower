(function(){
	var app = angular.module('App');
	
	app.controller('ListController', ['$scope', '$controller', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'jfxTvShow', function($scope, $controller, DTOptionsBuilder, DTColumnDefBuilder, jfxTvShow){
		var controller = this;
		
		$controller('AsyncController', {$scope:$scope,controller:controller});
		
		controller.initValues = function() {
			controller.content = $scope.content;
			
			controller.dtOptions = DTOptionsBuilder.newOptions().withOption('order', [1, 'asc']);;
			controller.dtColumnDefs = [
			                           DTColumnDefBuilder.newColumnDef(0).notSortable(),
			                           DTColumnDefBuilder.newColumnDef(1),
			                           DTColumnDefBuilder.newColumnDef(2),
			                           DTColumnDefBuilder.newColumnDef(3),
			                           DTColumnDefBuilder.newColumnDef(4)
			                           ];
			
			controller.selectedTvShow = null;
			controller.lastEpisodeWatchedSeasonOptions = {
				min: 1,
				initval: 1,
				prefix: 'S'
			};
			controller.lastEpisodeWatchedNumberOptions = {
				min: 1,
				initval: 1,
				prefix: 'E'
			};
			
			$("#modalUpdateWatched input[type='text']:first").attr('id', 'txtLastEpisodeWatchedSeason');
			
			controller.masterResponse = undefined;
			controller.response = undefined;
			
			if(controller.content === 'all') {
				controller.title = 'view.list.allTvShows';
				controller.bottomLink = 'view.list.viewOutdatedTVShows';
				controller.bottomOutcome = 'overview';
			}
			else if(controller.content === 'outdated') {
				controller.title = 'view.list.outdated';
				controller.bottomLink = 'view.list.viewAllTVShows';
				controller.bottomOutcome = 'all-tv-shows';
			}
			
			controller.loadContent();
		};
		
		controller.loadContent = function() {
			if(controller.content === 'all') {
				controller.tvShows = JSON.parse(jfxTvShow.getAll());
			}
			else if(controller.content === 'outdated') {
				controller.tvShows = JSON.parse(jfxTvShow.getOutdated());
			}
		};
		
		controller.isOutdated = function() {
			return (controller.content === 'outdated');
		};
		controller.viewIMDbPage = function(tvShow) {
			jfxTvShow.viewIMDbPage(tvShow.id);
		};
		controller.searchTorrent = function(tvShow) {
			jfxTvShow.searchTorrent(tvShow.id);
		};
		
		controller.clearResponse = function() {
			controller.masterResponse = undefined;
			controller.response = undefined;
		}
		controller.launchLastWatched = function(tvShow) {
			controller.selectedTvShow = new TvShow(tvShow);
			//controller.selectedTvShow.lastEpisodeWatched = controller.selectedTvShow.nextEpisodeToWatch;
			$('#modalUpdateWatched').modal('show');
		};
		controller.saveLastWatched = function(clear) {
			var tvShow;
			
			if(clear == true) {
				tvShow = new TvShow(controller.selectedTvShow);
				tvShow.lastEpisodeWatched = undefined;
			}
			else {
				tvShow = controller.selectedTvShow;
			}
			
			var response = JSON.parse(jfxTvShow.saveWatched(tvShow));
			
			if(response.status == 'SUCCESS') {
				controller.response = undefined;
				controller.masterResponse = response;
				$scope.$broadcast('tv-show-updated');
				$('#modalUpdateWatched').modal('hide');
			}
			else {
				controller.response = response;
			}
		};
		
		$scope.$on('tv-show-updated', function(event, data){
			controller.loadContent();
		});
		
		controller.initValues();
	}]);
	
	app.directive('list', function(){
		return {
			restrict: 'A',
			templateUrl: 'view/list.html',
			controller: 'ListController',
			controllerAs: 'listCtrl',
			scope: {
				content: '@' // all | outdated
			}
		};
	});
}());