(function(){
	var app = angular.module('App');
	
	app.factory('customTranslateLoader', ['$q', 'jfxI18n', function($q, jfxI18n) {
		return function(options) {
			var deferred = $q.defer();
			var translations;
			
			translations = jfxI18n.getMessages(options.key);
			
			if(translations != '{}') {
				deferred.resolve(JSON.parse(translations));
			}
			else {
				deferred.reject(options.key);
			}
			
			return deferred.promise;
		};
	}]);
	
	app.controller('LanguageController', ['$state', '$translate', 'DTDefaultOptions', 'jfxI18n', '$scope', '$rootScope', function($state, $translate, DTDefaultOptions, jfxI18n, $scope, $rootScope) {
		var instance = this;
		
		instance.languages = JSON.parse(jfxI18n.getSupportedLanguages());
		var json = JSON.parse(jfxI18n.getLanguage());
		instance.tag = json.tag;
		
		reload();
		
		this.is = function(language) {
			return language.tag === this.tag;
		};
		this.set = function(language) {
			if(instance.tag != language.tag) {
				instance.tag = language.tag;
				jfxI18n.setLanguage(instance.tag);
				reload();
				// TODO Verificar necessidade
				$state.reload();
				$rootScope.$broadcast('i18n-reloaded');
			}
		};
		
		function reload() {
			$translate.use(instance.tag);
			DTDefaultOptions.setLanguageSource('resources/assets/datatables/language/' + instance.tag + '.json');
		}
	}]);
	
	app.directive('language', function(){
		return {
			restrict: 'A',
			templateUrl: 'view/language.html',
			controller: 'LanguageController',
			controllerAs: 'languageCtrl'
		};
	});
}());