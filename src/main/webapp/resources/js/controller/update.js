(function(){
	var app = angular.module('App');
	
	app.controller('UpdateController', ['$scope', '$controller', '$interval', 'jfxTvShow', function($scope, $controller, $interval, jfxTvShow){
		var controller = this;
		
		$controller('AsyncController', {$scope:$scope, controller:controller});
		$controller.active = ! jfxTvShow.isUpdating();
		controller.token = "1";
		
		controller.update = function(all) {
			$controller.active = true;
			jfxTvShow.update(all, controller.createNewToken());
		};
		controller.onResponse = function() {
			if(controller.response.status == 'SUCCESS') {
				$scope.$broadcast('tv-show-updated');
			}
		};
		
		// Overrides
		controller.createNewToken = function() {
			controller.clearResponse();
			return controller.token;
		};
		controller.isActive = function() {
			return controller.active;
		};
		$interval(function(){
			var active = ! jfxTvShow.isUpdating();
			if(controller.active != active) {
				controller.active = active;
				$scope.$apply();
			}
		}, 200, 0, false);
	}]);
	
	app.directive('update', function(){
		return {
			restrict: 'A',
			templateUrl: 'view/update.html',
			controller: 'UpdateController',
			controllerAs: 'updateCtrl'
		};
	})
}());