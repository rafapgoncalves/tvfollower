(function(){
	var app = angular.module('App');
	
	app.controller('AsyncController', ['$scope', 'controller', function($scope, controller){
		controller.token = undefined;
		controller.response = undefined;
		
		controller.createNewToken = function() {
			controller.clearResponse();
			controller.token = "" + Date.now();
			return controller.token;
		};
		controller.clearResponse = function() {
			controller.response = undefined;
		};
		controller.isActive = function() {
			return controller.token === undefined;
		}
		
		controller.hasError = function(field) {
			var found = false;
			
			if(controller.response && controller.response.errors) {
				$.each(controller.response.errors, function(key, value){
					if(field === value.field) {
						found = true;
						return false;
					}
				});
			}
			
			return found;
		};
		controller.getError = function(field) {
			var errorMessage = undefined;
			
			if(controller.response && controller.response.errors) {
				$.each(controller.response.errors, function(key, value){
					if(field === value.field) {
						errorMessage = value.message;
						return false;
					}
				});
			}
			
			return errorMessage;
		};
		
		$scope.$on('jfxcallback', function(event, data) {
			if(data.token === controller.token) {
				data.listened = true;
				controller.response = data;
				controller.token = undefined;
				
				if(controller.onResponse) {
					controller.onResponse();
				}
				
				$scope.$apply();
			}
		});
	}]);
}());