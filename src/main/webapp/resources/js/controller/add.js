(function(){
	var app = angular.module('App');
	
	app.controller('AddController', ['$scope', '$controller', 'jfxTvShow', function($scope, $controller, jfxTvShow){
		var controller = this;
		
		$controller('AsyncController', {$scope:$scope, controller:controller});
		
		controller.imdbUrl = 'http://www.imdb.com/title/tt3107288/?ref_=nv_sr_1';
		
		controller.add = function() {
			jfxTvShow.add(controller.imdbUrl, controller.createNewToken());
		};
		controller.onResponse = function() {
			if(controller.response.status == 'SUCCESS') {
				$scope.$broadcast('tv-show-updated');
			}
		};
	}]);
	
	app.directive('add', function(){
		return {
			restrict: 'A',
			templateUrl: 'view/add.html',
			controller: 'AddController',
			controllerAs: 'addCtrl'
		};
	});
}());