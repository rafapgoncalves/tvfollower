(function(){
	var javaFXModule = angular.module('jfxModule', []);
	
	javaFXModule.factory('jfxCallback', function($rootScope) {
		var jfxCallback = {
			broadcast: function(json) {
				$rootScope.$broadcast('jfxcallback', json);
				if(! json.listened === true) {
					$rootScope.$broadcast('jfxcallback-orphaned', json);
				}
			}
		};
		
		window.callback = function(json) {
			jfxCallback.broadcast(json);
		};
		
		return jfxCallback;
	}).run(function(jfxCallback){});
	
	var javaServices = ['i18n', 'tvShow'];
	
	$.each(javaServices, function(key,value) {
		var service = value.charAt(0).toUpperCase() + value.slice(1);
		
		javaFXModule.factory('jfx' + service, function(){
			return javaFXBridge.get(value);
		});
	});
}());