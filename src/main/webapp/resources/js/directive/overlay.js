(function(){
	var app = angular.module('App');
	
	app.directive('overlay', function(){
		return {
			restrict: 'A',
			templateUrl: 'view/overlay.html'
		};
	});
}());