(function(){
	var app = angular.module('App', ['ui.router', 'oc.lazyLoad', 'angular-loading-bar', 'pascalprecht.translate', 'datatables', 'rafax.touchspin', 'jfxModule']);
	
	app.config(['$stateProvider', '$urlRouterProvider', '$translateProvider', function($stateProvider, $urlRouterProvider, $translateProvider){
		$urlRouterProvider.otherwise('/overview');
		
		$stateProvider
			.state('overview',{
				url: '/overview',
				templateUrl: 'view/overview.html',
				resolve: {
					loadScripts: ['$ocLazyLoad', function($ocLazyLoad){
						return $ocLazyLoad.load({
							series: true,
							files: ['resources/css/overview.css',
							        'resources/js/controller/update.js',
							        'resources/js/controller/list.js',
							        'resources/js/controller/add.js']
						});
					}]
				}
			})
			.state('all-tv-shows',{
				url: '/all-tv-shows',
				templateUrl: 'view/all-tv-shows.html',
				resolve: {
					loadScripts: ['$ocLazyLoad', function($ocLazyLoad){
						return $ocLazyLoad.load({
							series: true,
							files: ['resources/js/controller/list.js']
						});
					}]
				}
			});
		
		$translateProvider.preferredLanguage('en_US');
		$translateProvider.useLoader('customTranslateLoader');
	}]);
}());