package com.rafapgoncalves.tvfollower;

import java.util.LinkedHashSet;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class TvShow {
	private String id;
	private String name;
	private boolean watching;
	private boolean ended;
	@JsonDeserialize(as=LinkedHashSet.class)
	private Set<Episode> lastEpisodes = new LinkedHashSet<Episode>();
	private Episode lastEpisodeWatched;
	
	public Episode getLastEpisode() {
		Episode episode = null;
		
		for(Episode lastEpisode : lastEpisodes) {
			if(episode == null || episode.getSeason() < lastEpisode.getSeason()) {
				episode = lastEpisode;
			}
		}
		
		return episode;
	}
	public int getOutdatedEpisodeCount() {
		int total = 0;
		
		for(Episode lastEpisode : lastEpisodes) {
			if(lastEpisodeWatched != null && lastEpisode.getSeason() == lastEpisodeWatched.getSeason()) {
				if(lastEpisode.getNumber() > lastEpisodeWatched.getNumber()) {
					total += lastEpisode.getNumber() - lastEpisodeWatched.getNumber();
				}
			}
			else if(lastEpisodeWatched == null || lastEpisode.getSeason() > lastEpisodeWatched.getSeason()) {
				total += lastEpisode.getNumber();
			}
		}
		
		return total;
	}
	public Episode getNextEpisodeToWatch() {
		if(lastEpisodeWatched == null) {
			return new Episode(1, 1);
		}
		else {
			Episode lastEpisodeOfSeason = getLastEpisodeOfSeason(lastEpisodeWatched.getSeason());
			
			
			if(lastEpisodeOfSeason != null) {
				if(lastEpisodeOfSeason.getNumber() == lastEpisodeWatched.getNumber() && getLastEpisodeOfSeason(lastEpisodeWatched.getSeason() + 1) != null) {
					return new Episode(lastEpisodeWatched.getSeason() + 1, 1);
				}
			}
			
			return new Episode(lastEpisodeWatched.getSeason(), lastEpisodeWatched.getNumber() + 1);
		}
	}
	private Episode getLastEpisodeOfSeason(int season) {
		for(Episode lastEpisode : lastEpisodes) {
			if(lastEpisode.getSeason() == season) {
				return lastEpisode;
			}
		}
		
		return null;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isWatching() {
		return watching;
	}
	public void setWatching(boolean watching) {
		this.watching = watching;
	}
	public boolean isEnded() {
		return ended;
	}
	public void setEnded(boolean ended) {
		this.ended = ended;
	}
	public Set<Episode> getLastEpisodes() {
		return lastEpisodes;
	}
	public void setLastEpisodes(Set<Episode> lastEpisodes) {
		this.lastEpisodes = lastEpisodes;
	}
	public Episode getLastEpisodeWatched() {
		return lastEpisodeWatched;
	}
	public void setLastEpisodeWatched(Episode lastEpisodeWatched) {
		this.lastEpisodeWatched = lastEpisodeWatched;
	}
}
