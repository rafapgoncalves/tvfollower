package com.rafapgoncalves.tvfollower.ui.i18n;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rafapgoncalves.tvfollower.i18n.I18nService;
import com.rafapgoncalves.tvfollower.i18n.Language;
import com.rafapgoncalves.tvfollower.ui.Bridge;
import com.rafapgoncalves.tvfollower.ui.JavaGateway;
import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;

@Controller("i18nController")
public class I18nBridge implements Bridge {
	@Autowired
	private I18nService i18nService;
	@Autowired
	private MessageConverter messageConverter;
	
	public String getSupportedLanguages() {
		List<Language> supportedLanguages = i18nService.getSupportedLanguages();
		
		return messageConverter.toJson(supportedLanguages);
	}
	public String getLanguage() {
		Language language;
		
		language = i18nService.getLanguage();
		
		return messageConverter.toJson(language);
	}
	public void setLanguage(String tag) {
		Language language = getSupportedLanguage(tag);
		
		if(language != null) {
			i18nService.setLanguage(language);
		}
	}
	private Language getSupportedLanguage(String tag) {
		for(Language language : i18nService.getSupportedLanguages()) {
			if(language.getLocale().toString().equals(tag)) {
				return language;
			}
		}
		
		return null;
	}
	public String getMessages(String tag) {
		Language language = getSupportedLanguage(tag);
		
		if(language != null) {
			MessagesDTO messages = new MessagesDTO();
			
			addMessage(messages, "model.tvShow.name", language);
			addMessage(messages, "model.tvShow.watching", language);
			addMessage(messages, "model.tvShow.ended", language);
			addMessage(messages, "model.tvShow.lastEpisodeWatched", language);
			
			addMessage(messages, "view.update.updateAll", language);
			addMessage(messages, "view.update.update", language);
			
			addMessage(messages, "view.list.outdated", language);
			addMessage(messages, "view.list.allTvShows", language);
			addMessage(messages, "view.list.released", language);
			addMessage(messages, "view.list.watched", language);
			addMessage(messages, "view.list.viewIMDbPage", language);
			addMessage(messages, "view.list.searchEzTv", language);
			addMessage(messages, "view.list.notWatching", language);
			addMessage(messages, "view.list.viewAllTVShows", language);
			addMessage(messages, "view.list.viewOutdatedTVShows", language);
			addMessage(messages, "view.list.clear", language);
			addMessage(messages, "view.list.save", language);
			
			addMessage(messages, "view.add.addTVShow", language);
			addMessage(messages, "view.add.imdbUrl", language);
			addMessage(messages, "view.add.add", language);
			
			return messageConverter.toJson(messages);
		}
		else {
			return "{}";
		}
	}
	private void addMessage(MessagesDTO messages, String code, Language language) {
		messages.put(code, i18nService.get(code, language));
	}
	
	@Override
	public void setGateway(JavaGateway gateway) {
	}
	@Override
	public String getName() {
		return "i18n";
	}
}
