package com.rafapgoncalves.tvfollower.ui.i18n;

import java.util.HashMap;

@SuppressWarnings("serial")
class MessagesDTO extends HashMap<String, Object> {
	
	@Override
	public Object put(String key, Object value) {
		if(! (value instanceof String)) {
			throw new RuntimeException("Value is not a String");
		}
		
		if(! key.contains(".")) {
			if(! this.containsKey(key) || ! (this.get(key) instanceof MessagesDTO)) {
				return super.put(key, value);
			}
			else {
				key += ".";
			}
		}
		
		String thisKey, followKey;
		
		thisKey = key.substring(0, key.indexOf("."));
		followKey = key.substring(key.indexOf(".") + 1);
		
		MessagesDTO thisValue;
		
		if(this.containsKey(thisKey) && this.get(thisKey) instanceof MessagesDTO) {
			thisValue = (MessagesDTO) this.get(thisKey);
		}
		else {
			thisValue = new MessagesDTO();
			
			if(this.containsKey(thisKey)) {
				thisValue.put("", this.get(thisKey));
			}
			
			super.put(thisKey, thisValue);
		}
		
		return thisValue.put(followKey, value);
	}
}
