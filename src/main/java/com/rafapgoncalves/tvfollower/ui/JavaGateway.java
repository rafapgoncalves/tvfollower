package com.rafapgoncalves.tvfollower.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;
import com.rafapgoncalves.utils.javafx.JavaFXWrapper;

public class JavaGateway {
	private JavaFXWrapper javaFXWrapper;
	@Autowired(required=false)
	private List<Bridge> bridges;
	@Autowired
	private MessageConverter messageConverter;
	
	public JavaGateway(JavaFXWrapper javaFXWrapper) {
		this.javaFXWrapper = javaFXWrapper;
	}
	
	@PostConstruct
	private void postConstruct() {
		bridges = (bridges != null) ? bridges : new ArrayList<Bridge>();
		
		for(Bridge bridge : bridges) {
			bridge.setGateway(this);
		}
	}
	
	public <T> void convertFromJS(Object jsObject, T object, Errors errors) {
		javaFXWrapper.loadJSObject(jsObject, object, errors);
	}
	
	public void makeRequest(final AsynchRequest asynchRequest) {
		Runnable thread = new Runnable() {
			@Override
			public void run() {
				final RequestResult result = asynchRequest.execute();
				
				final String json = messageConverter.toJson(result);
				
				javaFXWrapper.runLater(new Runnable() {
					@Override
					public void run() {
						javaFXWrapper.executeScript("window.callback(" + json + ")");
					}
				});
			}
		};
		
		new Thread(thread).start();
	}
	public Bridge get(String name) {
		for(Bridge bridge : bridges) {
			if(bridge.getName().equals(name)) {
				return bridge;
			}
		}
		
		return null;
	}
}
