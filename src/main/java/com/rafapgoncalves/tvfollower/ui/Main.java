package com.rafapgoncalves.tvfollower.ui;

import java.awt.SplashScreen;

import com.rafapgoncalves.tvfollower.App;
import com.rafapgoncalves.utils.javafx.JavaFXWrapper;
import com.rafapgoncalves.utils.javafx.fonts.FontAwesomeImport;

public class Main extends JavaFXWrapper {
	private static final SplashScreen splash = SplashScreen.getSplashScreen();
	
	private JavaGateway gateway;
	
	public Main() {
		super("TvFollower", "index.html", new FontAwesomeImport("/resources/bower_components"));
		
		gateway = new JavaGateway(this);
		App.init(gateway);
	}
	
	@Override
	protected void doStart() {
		setJSObject("javaFXBridge", gateway);
	}
	@Override
	protected void postStart() {
		if(splash != null) {
			splash.close();
		}
	}
	
	public static void main(String[] args) {
		JavaFXWrapper.launcher(Main.class, args);
	}
}
