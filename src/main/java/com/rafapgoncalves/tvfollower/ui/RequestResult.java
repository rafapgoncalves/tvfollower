package com.rafapgoncalves.tvfollower.ui;

import org.springframework.validation.Errors;

import com.rafapgoncalves.utils.Result;

public class RequestResult {
	private String token;
	private Result result;
	private Errors errors;
	
	public RequestResult(String token, Result result, Errors errors) {
		this.token = token;
		this.result = result;
		this.errors = errors;
	}
	
	public String getToken() {
		return token;
	}
	public Result getResult() {
		return result;
	}
	public Errors getErrors() {
		return errors;
	}
}
