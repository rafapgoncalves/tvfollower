package com.rafapgoncalves.tvfollower.ui;

public interface Bridge {
	public void setGateway(JavaGateway gateway);
	public String getName();
}
