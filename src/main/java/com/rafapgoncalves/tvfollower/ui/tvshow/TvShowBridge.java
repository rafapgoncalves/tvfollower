package com.rafapgoncalves.tvfollower.ui.tvshow;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.tvshow.TvShowService;
import com.rafapgoncalves.tvfollower.ui.AsynchRequest;
import com.rafapgoncalves.tvfollower.ui.Bridge;
import com.rafapgoncalves.tvfollower.ui.JavaGateway;
import com.rafapgoncalves.tvfollower.ui.RequestResult;
import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;
import com.rafapgoncalves.utils.Result;

@Controller("tvShowBridge")
public class TvShowBridge implements Bridge {
	private static final String IMDB_URL = "http://www.imdb.com/title/${id}";
	//private static final String EXTRA_TORRENT_URL = "http://extratorrent.cc/search/?new=1&search=${search}&s_cat=8";
	private static final String EZTV_URL = "https://eztv.ag/search/${search}";
	
	@Autowired
	private TvShowService tvShowService;
	@Autowired
	private MessageConverter messageConverter;
	private JavaGateway gateway;
	
	public void viewIMDbPage(final String id) {
		Thread thread = new Thread(new Runnable(){
			@Override
			public void run() {
				Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
				if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
					try {
						desktop.browse(new URL(IMDB_URL.replace("${id}", id)).toURI());
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}});
		
		thread.start();
	}
	public void searchTorrent(final String id) {
		TvShow tvShow = tvShowService.getTvShowById(id);
		final String search = (tvShow.getName() + " " + tvShow.getNextEpisodeToWatch().toString()).replace(" ", "-");
		
		Thread thread = new Thread(new Runnable(){
			@Override
			public void run() {
				Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
				if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
					try {
						String encoded = URLEncoder.encode(search, "UTF-8");
						
						desktop.browse(new URL(EZTV_URL.replace("${search}", encoded)).toURI());
					}
					catch (IOException | URISyntaxException e) {
						e.printStackTrace();
					}
				}
			}});
		
		thread.start();
	}
	
	public String getAll() {
		List<TvShow> tvShows = tvShowService.getTvShows();
		
		return messageConverter.toJson(tvShows);
	}
	public String getOutdated() {
		List<TvShow> tvShows = tvShowService.getOutdatedTvShows();
		
		return messageConverter.toJson(tvShows);
	}
	public void add(final String url, final String token) {
		gateway.makeRequest(new AsynchRequest() {
			@Override
			public RequestResult execute() {
				Errors errors = new BeanPropertyBindingResult(url, "url");
				Result result = tvShowService.add(url, errors);
				
				return new RequestResult(token, result, errors);
			}
		});
	}
	public boolean isUpdating() {
		return tvShowService.isUpdating();
	}
	public void update(final boolean all, final String token) {
		gateway.makeRequest(new AsynchRequest() {
			@Override
			public RequestResult execute() {
				Result result = tvShowService.update(all);
				
				return new RequestResult(token, result, null);
			}
		});
	}
	public String saveWatched(Object jsTvShow) {
		TvShow tvShow = new TvShow();
		Errors errors = new BeanPropertyBindingResult(tvShow, "tvShow");
		
		gateway.convertFromJS(jsTvShow, tvShow, errors);
		
		Result result = tvShowService.saveWatched(tvShow, errors);
		
		return messageConverter.toJson(new RequestResult("", result, errors));
	}
	
	@Override
	public void setGateway(JavaGateway gateway) {
		this.gateway = gateway;
	}
	@Override
	public String getName() {
		return "tvShow";
	}
}
