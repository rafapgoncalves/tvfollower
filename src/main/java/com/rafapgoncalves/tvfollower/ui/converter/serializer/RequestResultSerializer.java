package com.rafapgoncalves.tvfollower.ui.converter.serializer;

import java.lang.reflect.Type;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.rafapgoncalves.tvfollower.i18n.I18nService;
import com.rafapgoncalves.tvfollower.ui.RequestResult;
import com.rafapgoncalves.tvfollower.ui.converter.MessageSerializer;

@Component("requestResultMessageSerializer")
class RequestResultSerializer implements MessageSerializer<RequestResult> {
	@Autowired
	private I18nService i18nService;
	
	@Override
	public JsonElement serialize(RequestResult requestResult, Type type, JsonSerializationContext context) {
		final JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("token", requestResult.getToken());
		jsonObject.addProperty("status", requestResult.getResult().getStatus().name());
		jsonObject.addProperty("message", i18nService.get(requestResult.getResult().getMessageCode(), requestResult.getResult().getMessageArgs()));
		
		if(requestResult.getErrors() != null && requestResult.getErrors().hasErrors()) {
			Errors errors = requestResult.getErrors();
			JsonArray jsonErrors = new JsonArray();
			
			if(errors.hasGlobalErrors()) {
				ObjectError error = errors.getGlobalError();
				JsonObject jsonError = new JsonObject();
				
				jsonError.addProperty("message", i18nService.get(error.getCode(), error.getArguments()));
				
				jsonErrors.add(jsonError);
			}
			
			if(errors.hasFieldErrors()) {
				for(FieldError error : errors.getFieldErrors()) {
					JsonObject jsonError = new JsonObject();
					
					jsonError.addProperty("field", error.getField());
					jsonError.addProperty("message", i18nService.get(error.getCode(), error.getArguments()));
					
					jsonErrors.add(jsonError);
				}
			}
			
			jsonObject.add("errors", context.serialize(jsonErrors));
		}
		
		return jsonObject;
	}
	@Override
	public Class<RequestResult> getMessageClass() {
		return RequestResult.class;
	}
}
