package com.rafapgoncalves.tvfollower.ui.converter;

import com.google.gson.JsonSerializer;

public interface MessageSerializer<T> extends JsonSerializer<T> {
	public Class<T> getMessageClass();
}
