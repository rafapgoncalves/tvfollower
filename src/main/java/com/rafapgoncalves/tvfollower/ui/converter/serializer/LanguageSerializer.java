package com.rafapgoncalves.tvfollower.ui.converter.serializer;

import java.lang.reflect.Type;

import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.rafapgoncalves.tvfollower.i18n.Language;
import com.rafapgoncalves.tvfollower.ui.converter.MessageSerializer;

@Component("languageMessageSerializer")
class LanguageSerializer implements MessageSerializer<Language> {
	@Override
	public JsonElement serialize(Language language, Type type, JsonSerializationContext context) {
		final JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("tag", language.getLocale().toString());
		jsonObject.addProperty("name", language.getName());
		
		return jsonObject;
	}
	@Override
	public Class<Language> getMessageClass() {
		return Language.class;
	}
}
