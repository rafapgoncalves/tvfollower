package com.rafapgoncalves.tvfollower.ui.converter.serializer;

import java.lang.reflect.Type;

import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.ui.converter.MessageSerializer;

@Component("tvShowMessageSerializer")
class TvShowSerializer implements MessageSerializer<TvShow> {
	@Override
	public JsonElement serialize(TvShow tvShow, Type typeOfSrc, JsonSerializationContext context) {
		final JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("id", tvShow.getId());
		jsonObject.addProperty("name", tvShow.getName());
		jsonObject.addProperty("watching", tvShow.isWatching());
		jsonObject.addProperty("ended", tvShow.isEnded());
		jsonObject.add("lastEpisodeWatched", context.serialize(tvShow.getLastEpisodeWatched()));
		jsonObject.add("lastEpisode", context.serialize(tvShow.getLastEpisode()));
		jsonObject.addProperty("outdatedEpisodeCount", tvShow.getOutdatedEpisodeCount());
		jsonObject.add("nextEpisodeToWatch", context.serialize(tvShow.getNextEpisodeToWatch()));
		
		return jsonObject;
	}
	@Override
	public Class<TvShow> getMessageClass() {
		return TvShow.class;
	}
}
