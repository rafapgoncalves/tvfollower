package com.rafapgoncalves.tvfollower.ui.converter;

public interface MessageConverter {
	public String toJson(Object message);
}
