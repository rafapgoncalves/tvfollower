package com.rafapgoncalves.tvfollower.ui.converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component("messageConverter")
class MessageConverterImpl implements MessageConverter {
	@Autowired(required=false)
	private List<MessageSerializer<?>> messageSerializers;
	
	private final GsonBuilder gsonBuilder = new GsonBuilder();
	private Gson gson;
	
	@PostConstruct
	private void postConstruct() {
		messageSerializers = (messageSerializers != null) ? messageSerializers : new ArrayList<MessageSerializer<?>>();
		
		for(MessageSerializer<?> messageSerializer : messageSerializers) {
			gsonBuilder.registerTypeAdapter(messageSerializer.getMessageClass(), messageSerializer);
		}
		
		gson = gsonBuilder.create();
	}
	
	@Override
	public String toJson(Object message) {
		return gson.toJson(message);
	}
}
