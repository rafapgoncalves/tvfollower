package com.rafapgoncalves.tvfollower.ui;

public interface AsynchRequest {
	public RequestResult execute();
}
