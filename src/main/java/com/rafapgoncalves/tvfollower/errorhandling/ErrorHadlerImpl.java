package com.rafapgoncalves.tvfollower.errorhandling;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("errorHandler")
class ErrorHadlerImpl implements ErrorHandler {
	Logger log = LoggerFactory.getLogger(getClass().getName());
	
	@PostConstruct
	private void postConstruct() {
		Thread.setDefaultUncaughtExceptionHandler(this);
	}
	
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		log.error("Exception caught by error handler", e);
	}
}
