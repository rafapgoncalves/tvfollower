package com.rafapgoncalves.tvfollower.errorhandling;

import java.lang.Thread.UncaughtExceptionHandler;

public interface ErrorHandler extends UncaughtExceptionHandler {
}
