package com.rafapgoncalves.tvfollower;

public class Episode implements Comparable<Episode> {
	private int season;
	private int number;
	
	public Episode() {
	}
	public Episode(int season, int episode) {
		setSeason(season);
		setNumber(episode);
	}
	
	@Override
	public String toString() {
		return "S" +
				((getSeason() < 10) ? "0" : "") +
				String.valueOf(getSeason()) +
				"E" + 
				((getNumber() < 10) ? "0" : "") +
				String.valueOf(getNumber());
	}
	@Override
	public int compareTo(Episode o) {
		if(o == null) {
			return 1;
		}
		
		if(getSeason() != o.getSeason()) {
			return getSeason() - o.getSeason();
		}
		else {
			return getNumber() - o.getNumber();
		}
	}
	
	public int getSeason() {
		return season;
	}
	public void setSeason(int season) {
		this.season = season;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
}
