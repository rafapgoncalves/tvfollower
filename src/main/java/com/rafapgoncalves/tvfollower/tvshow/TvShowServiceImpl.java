package com.rafapgoncalves.tvfollower.tvshow;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
//import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.tvshow.data.TvShowImdbRepository;
import com.rafapgoncalves.tvfollower.tvshow.data.TvShowLocalRepository;
import com.rafapgoncalves.utils.Result;
import com.rafapgoncalves.utils.ResultStatus;

@Service("tvShowService")
class TvShowServiceImpl implements TvShowService {
	private static final String PATTERN_URL = "www.imdb.com/title/(tt\\d+)";
	
	// --
	private static final String MSG_ADD_SUCCESS = "service.tvShow.addSuccess";
	private static final String MSG_ADD_WARNING_EXISTING = "service.tvShow.addWarningExisting";
	private static final String MSG_ADD_WARNING_TIMEOUT = "service.tvShow.addWarningTimeout";
	private static final String MSG_ADD_ERROR = "service.tvShow.addError";
	
	private static final String ERR_INVALID_URL = "service.tvShow.errorInvalidUrl";
	
	// --
	private static final String MSG_SAVE_WATCHED_SUCCESS = "service.tvShow.saveWatchedSuccess";
	private static final String MSG_SAVE_WATCHED_WARNING = "service.tvShow.saveWatchedWarning";
	private static final String MSG_SAVE_WATCHED_ERROR = "service.tvShow.saveWatchedError";
	
	private static final String ERR_SEASON_RANGE = "service.tvShow.errorSeasonRange";
	private static final String ERR_NUMBER_RANGE = "service.tvShow.errorNumberRange";
	
	// --
	private static final String MSG_UPDATE_SUCCESS = "service.tvShow.updateSuccess";
	private static final String MSG_UPDATE_WARNING = "service.tvShow.updateWarning";
	private static final String MSG_UPDATE_WARNING_UPDATING = "service.tvShow.updateWarningUpdating";
	
	private Logger log = LoggerFactory.getLogger(TvShowServiceImpl.class);
	
	@Value("${app.crawler.threadPoolSize}")
	private int THREAD_POOL_SIZE;
	
	@Autowired
	private TvShowLocalRepository tvShowLocalRepository;
	@Autowired
	private TvShowImdbRepository tvShowImdbRepository;
	
	private AtomicBoolean updating = new AtomicBoolean(false);
	
	@Override
	public List<TvShow> getTvShows() {
		return tvShowLocalRepository.list();
	}
	@Override
	public TvShow getTvShowById(String id) {
		return tvShowLocalRepository.get(id);
	}
	@Override
	public List<TvShow> getOutdatedTvShows() {
		return tvShowLocalRepository.listOutdated();
	}
	@Override
	public Result add(String tvShowUrl, Errors errors) {
		Pattern pattern = Pattern.compile(PATTERN_URL);
		Matcher matcher = pattern.matcher(tvShowUrl);
		Result result;
		String id = null;
		
		if(matcher.find()) {
			id = matcher.group(1);
			TvShow tvShow;
			
			tvShow = tvShowLocalRepository.get(id);
			
			if(tvShow == null) {
				tvShow = tvShowImdbRepository.get(id);
				
				if(tvShow != null) {
					tvShow.setWatching(true);
					tvShow.setEnded(false);
					saveRelease(tvShow);
					result = new Result(ResultStatus.SUCCESS, MSG_ADD_SUCCESS);
				}
				else {
					result = new Result(ResultStatus.WARNING, MSG_ADD_WARNING_TIMEOUT);
				}
			}
			else {
				result = new Result(ResultStatus.WARNING, MSG_ADD_WARNING_EXISTING);
			}
		}
		else {
			errors.reject(ERR_INVALID_URL);
			result = new Result(ResultStatus.ERROR, MSG_ADD_ERROR);
		}
		
		log.info("Adding TvShow with id from url {} returned {}", id, result.getStatus());
		return result;
	}
	@Override
	public Result saveWatched(TvShow tvShow, Errors errors) {
		TvShow watched;
		Result result;
		
		watched = tvShowLocalRepository.get(tvShow.getId());
		
		if(watched == null) {
			errors.reject("", new Object[]{ tvShow.getId() }, null);
		}
		else {
			if(tvShow.getLastEpisodeWatched() != null) {
				errors.pushNestedPath("lastEpisodeWatched");
				
				if(! errors.hasFieldErrors("season") && tvShow.getLastEpisodeWatched().getSeason() <= 0) {
					errors.rejectValue("season", ERR_SEASON_RANGE);
				}
				
				if(! errors.hasFieldErrors("number") && tvShow.getLastEpisodeWatched().getNumber() <= 0) {
					errors.rejectValue("number", ERR_NUMBER_RANGE);
				}
				
				errors.popNestedPath();
			}
		}
		
		if(! errors.hasErrors()) {
			Episode oldWatched = watched.getLastEpisode();
			
			watched.setWatching(tvShow.isWatching());
			watched.setEnded(tvShow.isEnded());
			watched.setLastEpisodeWatched(tvShow.getLastEpisodeWatched());
			tvShowLocalRepository.save(watched);
			
			if(tvShow.getLastEpisodeWatched() == null || tvShow.getLastEpisodeWatched().compareTo(watched.getLastEpisode()) <= 0) {
				result = new Result(ResultStatus.SUCCESS, MSG_SAVE_WATCHED_SUCCESS);
			}
			else {
				result = new Result(ResultStatus.WARNING, MSG_SAVE_WATCHED_WARNING, new Object[] { tvShow.getLastEpisodeWatched().toString(), (oldWatched == null) ? "None" : oldWatched.toString() });
			}
		}
		else {
			result = new Result(ResultStatus.ERROR, MSG_SAVE_WATCHED_ERROR);
		}
		
		log.info("Saving last episode watched of TvShow id {} to {} returned {}", tvShow.getId(), tvShow.getLastEpisodeWatched(), result.getStatus());
		return result;
	}
	@Override
	public boolean isUpdating() {
		return updating.get();
	}
	@Override
	public Result update(boolean all) {
		Result result = null;
		
		if(! updating.get()) {
			updating.set(true);
			
			log.debug("Updating TvShows");
			
			List<TvShow> tvShows = tvShowLocalRepository.list();
			
			if(! all) {
				tvShows.removeIf(new Predicate<TvShow>() {
					@Override
					public boolean test(TvShow tvShow) {
						return (! tvShow.isWatching()) || tvShow.isEnded();
					}
				});
			}
			
			if(tvShows.size() > 0) {
				List<String> ids = new ArrayList<String>();
				
				for(TvShow tvShow : tvShows) {
					ids.add(tvShow.getId());
				}
				
				UpdateTask update = new UpdateTask();
				Pair<List<TvShow>, List<String>> pair = update.update(ids);
				
				for(TvShow tvShow : pair.getValue0()) {
					saveRelease(tvShow);
				}
				
				if(pair.getValue1().size() > 0) {
					List<String> tvShowsNames = new ArrayList<String>();
					
					for(String id : pair.getValue1()) {
						TvShow tvShow = tvShowLocalRepository.get(id);
						
						if(tvShow != null) {
							tvShowsNames.add(tvShow.getName());
						}
					}
					
					updating.set(false);
					result = new Result(ResultStatus.WARNING, MSG_UPDATE_WARNING, new Object[] {String.join(", ", tvShowsNames)});
				}
				
				log.debug("Updating TvShows saved {} tv shows and skipped {} tv shows", pair.getValue0().size(), pair.getValue1().size());
			}
			
			if(result == null) {
				updating.set(false);
				result = new Result(ResultStatus.SUCCESS, MSG_UPDATE_SUCCESS);
			}
		}
		else {
			result = new Result(ResultStatus.WARNING, MSG_UPDATE_WARNING_UPDATING);
		}
		
		log.info("TvShows update returned {}", result.getStatus());
		return result;
	}
	
	private void saveRelease(TvShow tvShow) {
		TvShow watched;
		
		watched = tvShowLocalRepository.get(tvShow.getId());
		
		if(watched != null) {
			tvShow.setWatching(watched.isWatching());
			tvShow.setEnded(watched.isEnded());
			tvShow.setLastEpisodeWatched(watched.getLastEpisodeWatched());
		}
		tvShowLocalRepository.save(tvShow);
		log.info("Updating last episode released of TvShow id {} to {}", tvShow.getId(), tvShow.getLastEpisode());
	}
	
	private class UpdateTask {
		private ExecutorService executor;
		private List<TvShow> gotten;
		private List<String> timeouts;
		
		public UpdateTask() {
			executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
			gotten = new CopyOnWriteArrayList<TvShow>();
			timeouts = new CopyOnWriteArrayList<String>();
		}
		
		public Pair<List<TvShow>, List<String>> update(List<String> ids) {
			for(final String id : ids) {
				Runnable thread = new Runnable() {
					@Override
					public void run() {
						TvShow tvShow = tvShowImdbRepository.get(id);
						
						if(tvShow != null) {
							gotten.add(tvShow);
						}
						else {
							timeouts.add(id);
						}
					}
				};
				
				executor.submit(thread);
			}
			
			executor.shutdown();
			
			while(! executor.isTerminated()) {
				try {
					Thread.sleep(200);
				}
				catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
			
			return new Pair<List<TvShow>, List<String>>(gotten, timeouts);
		}
	}
}
