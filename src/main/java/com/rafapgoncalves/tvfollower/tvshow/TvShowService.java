package com.rafapgoncalves.tvfollower.tvshow;

import java.util.List;

import org.springframework.validation.Errors;

import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.utils.Result;

public interface TvShowService {
	public List<TvShow> getTvShows();
	public TvShow getTvShowById(String id);
	public List<TvShow> getOutdatedTvShows();
	public Result add(String tvShowUrl, Errors errors);
	public Result saveWatched(TvShow tvShow, Errors errors);
	public boolean isUpdating();
	public Result update(boolean all);
}
