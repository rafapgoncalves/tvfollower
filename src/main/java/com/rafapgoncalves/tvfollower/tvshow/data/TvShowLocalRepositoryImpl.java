package com.rafapgoncalves.tvfollower.tvshow.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;

@Repository("tvShowLocalRepository")
class TvShowLocalRepositoryImpl implements TvShowLocalRepository {
	private Logger log = LoggerFactory.getLogger(TvShowLocalRepositoryImpl.class);
	
	@Value("${app.data.dbname}")
	private String dbname;
	
	private File db;
	private Map<String, TvShow> tvShows = new HashMap<String, TvShow>();
	
	@PostConstruct
	private void readDatabase() {
		log.debug("Detecting database file");
		
		final GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(TvShow.class, new TvShowDeserializer());
		final Gson gson = gsonBuilder.create();
		
		try {
			File folder = new File(TvShowLocalRepositoryImpl.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
			db = new File(folder.getPath() + File.separator + dbname);
			
			if(! db.exists()) {
				log.trace("Creating new database file");
				db.createNewFile();
			}
			
			if(db.length() > 0) {
				Reader reader = new FileReader(db);
				List<TvShow> tvShows = Arrays.asList(gson.fromJson(reader, TvShow[].class));
				reader.close();
				
				for(TvShow tvShow : tvShows) {
					this.tvShows.put(tvShow.getId(), tvShow);
				}
			}
			
			log.debug("Using database file {}", db.getPath());
		}
		catch (URISyntaxException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public long count() {
		return tvShows.size();
	}
	@Override
	public List<TvShow> list() {
		List<TvShow> tvShows = new ArrayList<TvShow>(this.tvShows.values());
		Collections.sort(tvShows, new Comparator<TvShow>() {
			@Override
			public int compare(TvShow show1, TvShow show2) {
				return show1.getName().compareTo(show2.getName());
			}
		});
		
		return clone(tvShows);
	}
	@Override
	public List<TvShow> listOutdated() {
		List<TvShow> tvShows = list();
		
		tvShows.removeIf(new Predicate<TvShow>() {
			@Override
			public boolean test(TvShow tvShow) {
				return (tvShow.getOutdatedEpisodeCount() == 0) || ! tvShow.isWatching();
			}
		});
		
		return tvShows;
	}
	@Override
	public TvShow get(String id) {
		return clone(tvShows.get(id));
	}
	@Override
	public void save(TvShow tvShow) {
		log.debug("Saving TvShow with id {} and name {}", tvShow.getId(), tvShow.getName());
		tvShows.put(tvShow.getId(), clone(tvShow));
		writeToDB();
	}
	@Override
	public void delete(String id) {
		log.debug("Deleting TvShow with id {}", id);
		tvShows.remove(id);
		writeToDB();
	}
	
	private TvShow clone(TvShow tvShow) {
		TvShow clone;
		
		if(tvShow == null) {
			return null;
		}
		
		clone = new TvShow();
		
		clone.setId(tvShow.getId());
		clone.setName(tvShow.getName());
		clone.setWatching(tvShow.isWatching());
		clone.setEnded(tvShow.isEnded());
		
		for(Episode episode : tvShow.getLastEpisodes()) {
			clone.getLastEpisodes().add(new Episode(episode.getSeason(), episode.getNumber()));
		}
		
		if(tvShow.getLastEpisodeWatched() != null) {
			clone.setLastEpisodeWatched(new Episode(tvShow.getLastEpisodeWatched().getSeason(), tvShow.getLastEpisodeWatched().getNumber()));
		}
		
		return clone;
	}
	private List<TvShow> clone(List<TvShow> tvShows) {
		for(int i = 0; i < tvShows.size(); i++) {
			tvShows.add(i, clone(tvShows.remove(i)));
		}
		
		return tvShows;
	}
	
	private synchronized void writeToDB() {
		List<TvShow> tvShows = list();
		
		final GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(TvShow.class, new TvShowSerializer());
		final Gson gson = gsonBuilder.create();
		
		try {
			log.debug("Writing changes to database file");
			Writer writer = new FileWriter(db);
			gson.toJson(tvShows, writer);
			
			writer.close();
			log.debug("Database changes written");
		}
		catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static class TvShowDeserializer implements JsonDeserializer<TvShow> {
		@Override
		public TvShow deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
			final JsonObject jsonObject = (JsonObject) json;
			final TvShow tvShow = new TvShow();
			
			tvShow.setId(jsonObject.get("id").getAsString());
			tvShow.setName(jsonObject.get("name").getAsString());
			tvShow.setWatching(jsonObject.has("watching") ? jsonObject.get("watching").getAsBoolean() : true);
			tvShow.setEnded(jsonObject.has("ended") ? jsonObject.get("ended").getAsBoolean() : false);
			tvShow.setLastEpisodes(new LinkedHashSet<Episode>());
			
			final JsonArray jsonEpisodesArray = jsonObject.get("lastEpisodes").getAsJsonArray();
			for(int i = 0; i < jsonEpisodesArray.size(); i++) {
				final JsonElement jsonEpisode = jsonEpisodesArray.get(i);
				tvShow.getLastEpisodes().add(context.deserialize(jsonEpisode, Episode.class));
			}
			tvShow.setLastEpisodeWatched(context.deserialize(jsonObject.get("lastEpisodeWatched"), Episode.class));
			
			return tvShow;
		}
	}
	private static class TvShowSerializer implements JsonSerializer<TvShow> {
		@Override
		public JsonElement serialize(TvShow tvShow, Type type, JsonSerializationContext context) {
			final JsonObject jsonObject = new JsonObject();
			
			jsonObject.addProperty("id", tvShow.getId());
			jsonObject.addProperty("name", tvShow.getName());
			jsonObject.addProperty("watching", tvShow.isWatching());
			jsonObject.addProperty("ended", tvShow.isEnded());
			
			final JsonArray jsonLastEpisodesArray = new JsonArray();
			for(final Episode episode : tvShow.getLastEpisodes()) {
				jsonLastEpisodesArray.add(context.serialize(episode));
			}
			jsonObject.add("lastEpisodes", jsonLastEpisodesArray);
			jsonObject.add("lastEpisodeWatched", context.serialize(tvShow.getLastEpisodeWatched()));
			
			return jsonObject;
		}
	}
}
