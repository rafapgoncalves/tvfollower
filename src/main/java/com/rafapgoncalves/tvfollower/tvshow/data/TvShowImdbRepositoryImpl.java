package com.rafapgoncalves.tvfollower.tvshow.data;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;

@Repository("tvShowImdbRepository")
class TvShowImdbRepositoryImpl implements TvShowImdbRepository {
	private static final String TV_SHOW_URL = "http://www.imdb.com/title/$ID$/";
	private static final String TV_SHOW_SEASON = "http://www.imdb.com/title/$ID$/episodes/_ajax?season=$SEASON$";
	
	private static Logger log = LoggerFactory.getLogger(TvShowImdbRepositoryImpl.class);
	
	@Value("${app.crawler.timeoutLength}")
	private int TIMEOUT_LENGTH;
	@Value("${app.crawler.timeoutLimit}")
	private int TIMEOUT_LIMIT;
	
	@Override
	public TvShow get(String id) {
		int timeouts = 0;
		TvShow tvShow = new TvShow();
		
		boolean indexFetched = false;
		boolean seasonsFetched = false;
		int maxSeason = 0;
		
		log.debug("Searching IMDb for TvShow id {}", id);
		
		while(! indexFetched && timeouts <= TIMEOUT_LIMIT) {
			try {
				Document index = Jsoup
						.connect(TV_SHOW_URL.replace("$ID$", id))
						.timeout(TIMEOUT_LENGTH)
						.userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0")
						.header("Accept-Language", "en")
						.get();
				
				Elements title = index.select(".titleBar .title_wrapper h1");
				
				tvShow.setId(id);
				tvShow.setName(title.text().replace((char) 160, (char) 32) .trim());
				maxSeason = Integer.parseInt(index.select("#title-episode-widget .seasons-and-year-nav div:nth-child(4)").select("a").get(0).text());
				
				indexFetched = true;
			}
			catch(IOException e) {
				timeouts++;
			}
		}
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy", new Locale("en_US"));
		int currentSeason = 1;
		
		while(! seasonsFetched && timeouts <= TIMEOUT_LIMIT) {
			try {
				seasonFor: for(int season = currentSeason; season <= maxSeason; season++) {
					Document page = Jsoup
							.connect(TV_SHOW_SEASON.replace("$ID$", id).replace("$SEASON$", String.valueOf(season)))
							.timeout(TIMEOUT_LENGTH)
							.userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0")
							.header("Accept-Language", "en")
							.get();
					
					Elements episodes = page.select(".list_item");
					
					for(int episode = 0; episode < episodes.size(); episode++) {
						String date = episodes.get(episode).select("div.airdate").text().replace(".", "");
						boolean endOfMonth = date.matches("^[A-Za-z]{3}\\.? [0-9]{4}$");
						boolean invalidDate = false;
						LocalDate releaseDate = null;
						
						try {
							if(endOfMonth) {
								date = "1 " + date;
							}
							
							releaseDate = LocalDate.parse(date, formatter);
							
							if(endOfMonth) {
								releaseDate = releaseDate.plusMonths(1).minusDays(1);
							}
						}
						catch(DateTimeParseException e) {
							invalidDate = true;
						}
						
						if(invalidDate || ChronoUnit.DAYS.between(releaseDate, LocalDate.now()) <= 0) {
							if(episode > 0) {
								tvShow.getLastEpisodes().add(new Episode(season, episode));
							}
							break seasonFor;
						}
					}
					
					tvShow.getLastEpisodes().add(new Episode(season, episodes.size()));
					currentSeason = season + 1;
				}
				
				seasonsFetched = true;
			}
			catch(IOException e) {
				timeouts++;
			}
		}
		
		if(timeouts > TIMEOUT_LIMIT) {
			log.debug("Timeout searching IMDb for TvShow id {}", id);
			return null;
		}
		else {
			log.debug("Success searching IMDb for TvShow id {}", id);
			return tvShow;
		}
	}
}
