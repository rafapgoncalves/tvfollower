package com.rafapgoncalves.tvfollower.tvshow.data;

import com.rafapgoncalves.tvfollower.TvShow;

public interface TvShowImdbRepository {
	public TvShow get(String id);
}
