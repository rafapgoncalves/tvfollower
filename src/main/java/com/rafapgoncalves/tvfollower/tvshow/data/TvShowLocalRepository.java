package com.rafapgoncalves.tvfollower.tvshow.data;

import java.util.List;

import com.rafapgoncalves.tvfollower.TvShow;

public interface TvShowLocalRepository {
	
	public long count();
	public List<TvShow> list();
	public List<TvShow> listOutdated();
	public TvShow get(String id);
	public void save(TvShow tvShow);
	public void delete(String id);
}
