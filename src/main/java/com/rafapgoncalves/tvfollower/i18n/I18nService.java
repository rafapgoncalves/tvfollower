package com.rafapgoncalves.tvfollower.i18n;

import java.util.List;

public interface I18nService {
	public List<Language> getSupportedLanguages();
	public Language getLanguage();
	public void setLanguage(Language language);
	
	public String get(String code);
	public String get(String code, Object[] args);
	public String get(String code, Language language);
	public String get(String code, Object[] args, Language language);
}
