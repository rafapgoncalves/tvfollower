package com.rafapgoncalves.tvfollower.i18n;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Service;

@Service("i18nService")
class I18nServiceImpl implements I18nService, MessageSourceAware {
	private static List<Language> SUPPORTED_LANGUAGES;
	private static Language DEFAULT_LANGUAGE;
	
	private Logger log = LoggerFactory.getLogger(I18nServiceImpl.class);
	
	private MessageSource messageSource;
	private Language language;
	
	static {
		DEFAULT_LANGUAGE = new Language("English", new Locale("en", "US"));
		
		SUPPORTED_LANGUAGES = new ArrayList<Language>();
		SUPPORTED_LANGUAGES.add(DEFAULT_LANGUAGE);
		SUPPORTED_LANGUAGES.add(new Language("Português", new Locale("pt", "BR")));
	}
	
	@PostConstruct
	private void postConstruct() {
		detectLanguage();
		log.debug("Language detected {}", language.getLocale());
	}
	private void detectLanguage() {
		Locale userLocale = Locale.getDefault();
		
		for(Language language : SUPPORTED_LANGUAGES) {
			if(language.getLocale().equals(userLocale)) {
				this.language = language;
				return;
			}
		}
		
		for(Language language : SUPPORTED_LANGUAGES) {
			if(userLocale.getLanguage().equals(language.getLocale().getLanguage()) && userLocale.getCountry().equals(language.getLocale().getCountry())) {
				this.language = language;
				return;
			}
		}
		
		for(Language language : SUPPORTED_LANGUAGES) {
			if(userLocale.getLanguage().equals(language.getLocale().getLanguage())) {
				this.language = language;
				return;
			}
		}
		
		for(Language language : SUPPORTED_LANGUAGES) {
			if(userLocale.getCountry().equals(language.getLocale().getCountry())) {
				this.language = language;
				return;
			}
		}
		
		language = DEFAULT_LANGUAGE;
	}
	
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	@Override
	public List<Language> getSupportedLanguages() {
		return Collections.unmodifiableList(SUPPORTED_LANGUAGES);
	}
	@Override
	public Language getLanguage() {
		return language;
	}
	@Override
	public void setLanguage(Language language) {
		if(! SUPPORTED_LANGUAGES.contains(language)) {
			throw new RuntimeException("Unsupported language");
		}
		
		this.language = language;
		log.debug("Language changed to {}", language.getLocale());
	}
	@Override
	public String get(String code) {
		return get(code, (Object[]) null);
	}
	@Override
	public String get(String code, Object[] args) {
		return messageSource.getMessage(code, args, language.getLocale());
	}
	@Override
	public String get(String code, Language language) {
		return messageSource.getMessage(code, null, language.getLocale());
	}
	@Override
	public String get(String code, Object[] args, Language language) {
		return messageSource.getMessage(code, args, language.getLocale());
	}
	
}
