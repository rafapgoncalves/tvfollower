package com.rafapgoncalves.tvfollower.i18n;

import java.util.Locale;

public class Language {
	private String name;
	private Locale locale;
	
	public Language(String name, Locale locale) {
		this.name = name;
		this.locale = locale;
	}
	
	public String getName() {
		return name;
	}
	public Locale getLocale() {
		return locale;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Language other = (Language) obj;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
