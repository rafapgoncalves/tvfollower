package com.rafapgoncalves.tvfollower;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.annotation.PostConstruct;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	private static ClassPathXmlApplicationContext context;
	
	public static void init(Object... targets) {
		if(context == null) {
			context = new ClassPathXmlApplicationContext(new String[] { "root-context.xml", "data-context.xml", "service-context.xml", "ui-context.xml" });
		}
		
		for(Object target : targets) {
			context.getAutowireCapableBeanFactory().autowireBean(target);
			
			Method[] methods = target.getClass().getDeclaredMethods();
			if(methods.length > 0) {
				for(Method method : methods) {
					if(method.isAnnotationPresent(PostConstruct.class) && method.getParameterCount() == 0) {
						try {
							if(! method.isAccessible()) {
								method.setAccessible(true);
							}
							
							method.invoke(target);
						}
						catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							throw new RuntimeException(e);
						}
					}
				}
			}
		}
	}
}
