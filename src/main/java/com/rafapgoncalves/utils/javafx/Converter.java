package com.rafapgoncalves.utils.javafx;

interface Converter<T> {
	public T convert(Object value);
}
