package com.rafapgoncalves.utils.javafx.fonts;

public class FontAwesomeImport extends CustomFontImport {
	private static final String INTERNAL_PATH = "/fontawesome/fonts";
	
	private String eotPath;
	private String woff2Path;
	private String woffPath;
	private String ttfPath;
	private String svgPath;
	
	public FontAwesomeImport(String basepath) {
		String base = getClass().getResource(basepath + INTERNAL_PATH).toExternalForm() + "/";
		eotPath = base + "fontawesome-webfont.eot";
		woff2Path = base + "fontawesome-webfont.woff2";
		woffPath = base + "fontawesome-webfont.woff";
		ttfPath = base + "fontawesome-webfont.ttf";
		svgPath = base + "fontawesome-webfont.svg";
	}
	
	@Override
	public String getCssDefinition() {
		return "@font-face {" + 
					"font-family: 'FontAwesome';" + 
					"src: url('" + eotPath + "');" + 
					"src: url('" + eotPath + "') format('embedded-opentype'), url('" + woff2Path + "') format('woff2'), url('" + woffPath + "') format('woff'), url('" + ttfPath + "') format('truetype'), url('" + svgPath + "') format('svg');" + 
					"font-weight: normal;" + 
					"font-style: normal;" + 
				"}";
	}
}
