package com.rafapgoncalves.utils.javafx;

class StringConverter implements Converter<String> {
	@Override
	public String convert(Object value) {
		return String.valueOf(value);
	}
}
