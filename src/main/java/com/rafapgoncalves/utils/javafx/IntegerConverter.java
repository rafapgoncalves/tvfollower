package com.rafapgoncalves.utils.javafx;

class IntegerConverter implements Converter<Integer> {
	@Override
	public Integer convert(Object value) {
		return Integer.parseInt(String.valueOf(value));
	}
}
