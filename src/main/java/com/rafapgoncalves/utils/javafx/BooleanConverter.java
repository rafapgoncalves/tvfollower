package com.rafapgoncalves.utils.javafx;

class BooleanConverter implements Converter<Boolean> {
	@Override
	public Boolean convert(Object value) {
		return Boolean.parseBoolean(String.valueOf(value));
	}
}
