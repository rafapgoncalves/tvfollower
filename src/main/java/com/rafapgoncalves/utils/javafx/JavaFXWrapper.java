package com.rafapgoncalves.utils.javafx;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.Errors;

import com.rafapgoncalves.utils.javafx.fonts.CustomFontImport;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;

@SuppressWarnings("restriction")
public abstract class JavaFXWrapper extends Application {
	private static Map<Class<?>, Converter<?>> editors;
	
	private String title;
	private String initialPage;
	private CustomFontImport[] fonts;
	private WebView webView;
	
	static {
		editors = new HashMap<Class<?>, Converter<?>>();
		editors.put(String.class, new StringConverter());
		editors.put(int.class, new IntegerConverter());
		editors.put(boolean.class, new BooleanConverter());
	}
	
	protected JavaFXWrapper(String title, String initialPage, CustomFontImport... fonts) {
		this.title = title;
		this.initialPage = initialPage;
		this.fonts = fonts;
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		webView = new WebView();
		
		if(fonts != null) {
			String customFonts = "data:,";
			
			for(CustomFontImport font : fonts) {
				customFonts += font.getCssDefinition();
			}
			
			webView.getEngine().setUserStyleSheetLocation(customFonts);
		}
		
		webView.getEngine().load(getClass().getResource("/" + initialPage).toURI().toURL().toString());
		doStart();
		
		Scene scene = new Scene(webView);
		primaryStage.setScene(scene);
		primaryStage.setMaximized(true);
		
		primaryStage.setTitle(title);
		primaryStage.show();
		
		postStart();
	}
	protected abstract void doStart();
	protected abstract void postStart();
	protected void setJSObject(String name, Object object) {
		JSObject obj = (JSObject) webView.getEngine().executeScript("window");
		obj.setMember(name, object);
	}
	public <T> void loadJSObject(Object jsObject, T object, Errors errors) {
		if(!(jsObject instanceof JSObject)) {
			throw new RuntimeException("Instance not a JSObject");
		}
		
		JSObject jsSource = (JSObject) jsObject;
		
		for(Field field : object.getClass().getDeclaredFields()) {
			if(! field.isAccessible()) {
				field.setAccessible(true);
			}
			
			if(editors.keySet().contains(field.getType())) {
				if(!"undefined".equals(jsSource.getMember(field.getName()))) {
					try {
						if(field.getType().equals(jsSource.getMember(field.getName()).getClass())) {
							field.set(object, jsSource.getMember(field.getName()));
						}
						else {
							field.set(object, editors.get(field.getType()).convert(jsSource.getMember(field.getName())));
						}
					}
					catch(Exception e) {
						errors.rejectValue(field.getName(), "TypeMismatch");
					}
				}
			}
			else if(!"undefined".equals(jsSource.getMember(field.getName()))) {
				errors.pushNestedPath(field.getName());
				
				try {
					field.set(object, field.getType().newInstance());
					loadJSObject(jsSource.getMember(field.getName()), field.get(object), errors);
				}
				catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
					throw new RuntimeException(e);
				}
				
				errors.popNestedPath();
			}
		}
	}
	
	public Object executeScript(String script) {
		return webView.getEngine().executeScript(script);
	}
	public void runLater(Runnable runnable) {
		Platform.runLater(runnable);
    }
	
	public static void launcher(Class<? extends JavaFXWrapper> clazz, String[] args) {
		Application.launch(clazz, args);
	}
}
