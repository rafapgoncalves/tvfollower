package com.rafapgoncalves.utils;

public class Result {
	private ResultStatus status;
	private String messageCode;
	private Object[] messageArgs;
	
	public Result(ResultStatus status, String messageCode) {
		this(status, messageCode, null);
	}
	public Result(ResultStatus status, String messageCode, Object[] messageArgs) {
		this.status = status;
		this.messageCode = messageCode;
		this.messageArgs = messageArgs;
	}
	
	public ResultStatus getStatus() {
		return status;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public Object[] getMessageArgs() {
		return messageArgs;
	}
}
