package com.rafapgoncalves.utils;

public enum ResultStatus {
	SUCCESS,
	WARNING,
	ERROR;
}
