package com.rafapgoncalves.tvfollower.ui.i18n;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

import com.rafapgoncalves.tvfollower.i18n.I18nService;
import com.rafapgoncalves.tvfollower.ui.JavaGateway;
import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;

@Configuration
@ComponentScan(
		basePackages={"com.rafapgoncalves.tvfollower.ui.i18n"},
		useDefaultFilters=false,
		includeFilters=@ComponentScan.Filter(value=Controller.class, type=FilterType.ANNOTATION))
class I18nBridgeContext {
	
	@Bean
	public static I18nService i18nService() {
		return Mockito.mock(I18nService.class);
	}
	@Bean
	public static JavaGateway javaGateway() {
		return Mockito.mock(JavaGateway.class);
	}
	@Bean
	public static MessageConverter messageConverter() {
		return Mockito.mock(MessageConverter.class);
	}
}
