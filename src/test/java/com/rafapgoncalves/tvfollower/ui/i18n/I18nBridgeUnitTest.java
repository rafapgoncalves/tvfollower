package com.rafapgoncalves.tvfollower.ui.i18n;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rafapgoncalves.tvfollower.AllUnitTestSuite;
import com.rafapgoncalves.tvfollower.i18n.I18nService;
import com.rafapgoncalves.tvfollower.i18n.Language;
import com.rafapgoncalves.tvfollower.ui.JavaGateway;
import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllUnitTestSuite.Context.class, I18nBridgeContext.class})
public class I18nBridgeUnitTest {
	
	@Autowired
	private I18nBridge i18nBridge;
	@Autowired
	private JavaGateway gateway;
	@Autowired
	private I18nService i18nService;
	@Autowired
	private MessageConverter messageConverter;
	
	@Before
	public void reset() {
		i18nBridge.setGateway(gateway);
		
		Mockito.reset(i18nService);
		Mockito.reset(gateway);
		Mockito.reset(messageConverter);
	}
	
	@Test
	public void getSupportedLanguages_shouldReturnJsonArrayAsString() throws Exception {
		String expectedJson;
		List<Language> supportedLanguages;
		
		expectedJson = "[{\"tag\":\"en_US\",\"name\":\"English\"},{\"tag\":\"pt_BR\",\"name\":\"Português\"}]";
		
		supportedLanguages = new ArrayList<Language>();
		supportedLanguages.add(new Language("English", new Locale("en", "US")));
		supportedLanguages.add(new Language("Português", new Locale("pt", "BR")));
		
		Mockito.when(i18nService.getSupportedLanguages()).thenReturn(supportedLanguages);
		Mockito.when(messageConverter.toJson(supportedLanguages)).thenReturn(expectedJson);
		
		String returnedJson = i18nBridge.getSupportedLanguages();
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verify(i18nService, Mockito.times(1)).getSupportedLanguages();
		Mockito.verifyNoMoreInteractions(i18nService);
		
		Mockito.verify(messageConverter, Mockito.times(1)).toJson(supportedLanguages);
		Mockito.verifyNoMoreInteractions(messageConverter);
		
		Mockito.verifyZeroInteractions(gateway);
		
		
	}
	@Test
	public void getLanguage_shouldReturnJsonObjectAsString() throws Exception {
		String expectedJson;
		Language language;
		
		expectedJson = "{\"tag\":\"en_US\",\"name\":\"English\"}";
		language = new Language("English", new Locale("en", "US"));
		
		Mockito.when(i18nService.getLanguage()).thenReturn(language);
		Mockito.when(messageConverter.toJson(language)).thenReturn(expectedJson);
		
		String returnedJson = i18nBridge.getLanguage();
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verify(i18nService, Mockito.times(1)).getLanguage();
		Mockito.verifyNoMoreInteractions(i18nService);
		
		Mockito.verify(messageConverter, Mockito.times(1)).toJson(language);
		Mockito.verifyNoMoreInteractions(messageConverter);
		
		Mockito.verifyZeroInteractions(gateway);
	}
	@Test
	public void setLanguage_shouldCallChangeLanguage() throws Exception {
		List<Language> supportedLanguages;
		
		supportedLanguages = new ArrayList<Language>();
		supportedLanguages.add(new Language("English", new Locale("en", "US")));
		supportedLanguages.add(new Language("Português", new Locale("pt", "BR")));
		
		Mockito.when(i18nService.getSupportedLanguages()).thenReturn(supportedLanguages);
		
		i18nBridge.setLanguage("pt_BR");
		
		ArgumentCaptor<Language> language = ArgumentCaptor.forClass(Language.class);
		
		Mockito.verify(i18nService, Mockito.times(1)).getSupportedLanguages();
		Mockito.verify(i18nService, Mockito.times(1)).setLanguage(language.capture());
		Mockito.verifyNoMoreInteractions(i18nService);
		
		Mockito.verifyZeroInteractions(messageConverter);
		Mockito.verifyZeroInteractions(gateway);
		
		Assert.assertEquals(supportedLanguages.get(1), language.getValue());
	}
	@Test
	public void setUnsupportedLanguage_shouldNotCallChangeLanguage() throws Exception {
		List<Language> supportedLanguages;
		
		supportedLanguages = new ArrayList<Language>();
		supportedLanguages.add(new Language("English", new Locale("en", "US")));
		supportedLanguages.add(new Language("Português", new Locale("pt", "BR")));
		
		Mockito.when(i18nService.getSupportedLanguages()).thenReturn(supportedLanguages);
		
		i18nBridge.setLanguage("fr_FR");
		
		Mockito.verify(i18nService, Mockito.times(1)).getSupportedLanguages();
		Mockito.verifyNoMoreInteractions(i18nService);
		
		Mockito.verifyZeroInteractions(messageConverter);
		Mockito.verifyZeroInteractions(gateway);
	}
	@Test
	public void getMessages_shouldReturnJsonObjectAsString() throws Exception {
		List<Language> supportedLanguages;
		
		supportedLanguages = new ArrayList<Language>();
		supportedLanguages.add(new Language("English", new Locale("en", "US")));
		supportedLanguages.add(new Language("Português", new Locale("pt", "BR")));
		
		String expectedJson = "{\"model\":{\"tvShow\":{\"name\":\"Name\"}}}";
		
		Mockito.when(i18nService.getSupportedLanguages()).thenReturn(supportedLanguages);
		Mockito.when(i18nService.get(Mockito.anyString(), Mockito.any(Language.class))).thenAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		});
		Mockito.when(messageConverter.toJson(Mockito.any(MessagesDTO.class))).thenReturn(expectedJson);
		
		String returnedJson = i18nBridge.getMessages("en_US");
		
		ArgumentCaptor<Language> language = ArgumentCaptor.forClass(Language.class);
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verify(i18nService, Mockito.times(1)).getSupportedLanguages();
		Mockito.verify(i18nService, Mockito.times(20)).get(Mockito.anyString(), language.capture());
		Mockito.verifyNoMoreInteractions(i18nService);
		
		Mockito.verify(messageConverter, Mockito.times(1)).toJson(Mockito.any(MessagesDTO.class));
		Mockito.verifyNoMoreInteractions(messageConverter);
		
		Mockito.verifyZeroInteractions(gateway);
		
		for(Language argument : language.getAllValues()) {
			Assert.assertEquals(supportedLanguages.get(0), argument);
		}
	}
	@Test
	public void getMessagesUnsupportedLanguage_shouldReturnEmptyJsonObjectAsString() throws Exception {
		List<Language> supportedLanguages;
		
		supportedLanguages = new ArrayList<Language>();
		supportedLanguages.add(new Language("English", new Locale("en", "US")));
		supportedLanguages.add(new Language("Português", new Locale("pt", "BR")));
		
		Mockito.when(i18nService.getSupportedLanguages()).thenReturn(supportedLanguages);
		
		String returnedJson = i18nBridge.getMessages("fr_FR");
		
		Assert.assertEquals("{}", returnedJson);
		
		Mockito.verify(i18nService, Mockito.times(1)).getSupportedLanguages();
		Mockito.verifyNoMoreInteractions(i18nService);
		
		Mockito.verifyZeroInteractions(messageConverter);
		
		Mockito.verifyZeroInteractions(gateway);
	}
}
