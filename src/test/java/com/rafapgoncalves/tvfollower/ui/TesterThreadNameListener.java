package com.rafapgoncalves.tvfollower.ui;

interface TesterThreadNameListener {
	public void listen(String name);
}
