package com.rafapgoncalves.tvfollower.ui;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rafapgoncalves.tvfollower.AllUnitTestSuite;
import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;
import com.rafapgoncalves.utils.Result;
import com.rafapgoncalves.utils.ResultStatus;
import com.rafapgoncalves.utils.javafx.JavaFXWrapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllUnitTestSuite.Context.class, JavaGatewayContext.class})
public class JavaGatewayUnitTest {
	@Autowired
	private JavaGateway javaGateway;
	@Autowired
	private JavaFXWrapper javaFXWrapper;
	@Autowired
	private TesterThreadNameListener testerThreadNameListener;
	@Autowired
	private MessageConverter messageConverter;
	
	@Before
	public void reset() {
		Mockito.reset(javaFXWrapper);
		Mockito.reset(testerThreadNameListener);
		Mockito.reset(messageConverter);
	}
	
	@Test
	public void makeRequest_shouldRunInAnotherThread() throws Exception {
		final RequestResult requestResult = new RequestResult("1", new Result(ResultStatus.SUCCESS, "code.message"), null);
		
		String requestResultJson = "{\"token\":\"1\",\"status\":\"SUCCESS\",\"message\":\"Message\"}";
		
		AsynchRequest request = new AsynchRequest() {
			@Override
			public RequestResult execute() {
				testerThreadNameListener.listen(Thread.currentThread().getName());
				
				return requestResult;
			}
		};
		Mockito.when(messageConverter.toJson(requestResult)).thenReturn(requestResultJson);
		Mockito.doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				new Thread((Runnable) invocation.getArgument(0)).start();
				
				return null;
			}
		}).when(javaFXWrapper).runLater(Mockito.any(Runnable.class));
		
		javaGateway.makeRequest(request);
		
		Thread.sleep(200);
		
		ArgumentCaptor<String> threadName = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> script = ArgumentCaptor.forClass(String.class);
		
		Mockito.verify(testerThreadNameListener, Mockito.times(1)).listen(threadName.capture());
		Mockito.verifyNoMoreInteractions(testerThreadNameListener);
		
		Mockito.verify(messageConverter, Mockito.times(1)).toJson(requestResult);
		Mockito.verifyNoMoreInteractions(messageConverter);
		
		Mockito.verify(javaFXWrapper, Mockito.times(1)).runLater(Mockito.any(Runnable.class));
		Mockito.verify(javaFXWrapper, Mockito.times(1)).executeScript(script.capture());
		Mockito.verifyNoMoreInteractions(javaFXWrapper);
		
		Assert.assertNotEquals(Thread.currentThread().getName(), threadName.getValue());
	}
}
