package com.rafapgoncalves.tvfollower.ui.tvshow;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.Errors;

import com.rafapgoncalves.tvfollower.AllUnitTestSuite;
import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.tvshow.TvShowService;
import com.rafapgoncalves.tvfollower.ui.AsynchRequest;
import com.rafapgoncalves.tvfollower.ui.JavaGateway;
import com.rafapgoncalves.tvfollower.ui.RequestResult;
import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;
import com.rafapgoncalves.utils.Result;
import com.rafapgoncalves.utils.ResultStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllUnitTestSuite.Context.class, TvShowBridgeContext.class})
public class TvShowBridgeUnitTest {
	@Autowired
	private TvShowBridge tvShowBridge;
	@Autowired
	private TvShowService tvShowService;
	@Autowired
	private JavaGateway gateway;
	@Autowired
	private MessageConverter messageConverter;
	
	@Before
	public void reset() {
		tvShowBridge.setGateway(gateway);
		
		Mockito.reset(tvShowService);
		Mockito.reset(gateway);
		Mockito.reset(messageConverter);
	}
	
	@Test
	public void getAll_shouldReturnJsonArrayAsString() throws Exception {
		String expectedJson;
		List<TvShow> tvShows = new ArrayList<TvShow>();
		
		tvShows.add(new TvShow());
		tvShows.get(tvShows.size() - 1).setId("tt2193021");
		tvShows.get(tvShows.size() - 1).setName("Arrow");
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(2, 23));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(3, 23));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(4, 21));
		tvShows.get(tvShows.size() - 1).setLastEpisodeWatched(new Episode(4, 21));
		
		tvShows.add(new TvShow());
		tvShows.get(tvShows.size() - 1).setId("tt2661044");
		tvShows.get(tvShows.size() - 1).setName("The 100");
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(1, 13));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(2, 16));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(3, 15));
		tvShows.get(tvShows.size() - 1).setLastEpisodeWatched(new Episode(3, 14));
		
		tvShows.add(new TvShow());
		tvShows.get(tvShows.size() - 1).setId("tt3107288");
		tvShows.get(tvShows.size() - 1).setName("The Flash");
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(2, 21));
		tvShows.get(tvShows.size() - 1).setLastEpisodeWatched(new Episode(2, 21));
		
		expectedJson = "[{\"id\":\"tt2193021\",\"name\":\"Arrow\",\"lastEpisodeWatched\":{\"season\":4,\"number\":21},\"lastEpisode\":{\"season\":4,\"number\":21},\"outdatedEpisodeCount\":0},{\"id\":\"tt2661044\",\"name\":\"The 100\",\"lastEpisodeWatched\":{\"season\":3,\"number\":14},\"lastEpisode\":{\"season\":3,\"number\":15},\"outdatedEpisodeCount\":1},{\"id\":\"tt3107288\",\"name\":\"The Flash\",\"lastEpisodeWatched\":{\"season\":2,\"number\":21},\"lastEpisode\":{\"season\":2,\"number\":21},\"outdatedEpisodeCount\":0}]";
		
		Mockito.when(tvShowService.getTvShows()).thenReturn(tvShows);
		Mockito.when(messageConverter.toJson(tvShows)).thenReturn(expectedJson);
		
		String returnedJson = tvShowBridge.getAll();
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verify(tvShowService, Mockito.times(1)).getTvShows();
		Mockito.verifyNoMoreInteractions(tvShowService);
		
		Mockito.verify(messageConverter, Mockito.times(1)).toJson(tvShows);
		Mockito.verifyNoMoreInteractions(messageConverter);
		
		Mockito.verifyZeroInteractions(gateway);
	}
	@Test
	public void getOutdated_shouldReturnJsonArrayAsString() throws Exception {
		String expectedJson;
		List<TvShow> tvShows = new ArrayList<TvShow>();
		
		tvShows.add(new TvShow());
		tvShows.get(tvShows.size() - 1).setId("tt2661044");
		tvShows.get(tvShows.size() - 1).setName("The 100");
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(1, 13));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(2, 16));
		tvShows.get(tvShows.size() - 1).getLastEpisodes().add(new Episode(3, 15));
		tvShows.get(tvShows.size() - 1).setLastEpisodeWatched(new Episode(3, 14));
		
		expectedJson = "[{\"id\":\"tt2661044\",\"name\":\"The 100\",\"lastEpisodeWatched\":{\"season\":3,\"number\":14},\"lastEpisode\":{\"season\":3,\"number\":15},\"outdatedEpisodeCount\":1}]";
		
		Mockito.when(tvShowService.getOutdatedTvShows()).thenReturn(tvShows);
		Mockito.when(messageConverter.toJson(tvShows)).thenReturn(expectedJson);
		
		String returnedJson = tvShowBridge.getOutdated();
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verify(tvShowService, Mockito.times(1)).getOutdatedTvShows();
		Mockito.verifyNoMoreInteractions(tvShowService);
		
		Mockito.verify(messageConverter, Mockito.times(1)).toJson(tvShows);
		Mockito.verifyNoMoreInteractions(messageConverter);
		
		Mockito.verifyZeroInteractions(gateway);
	}
	@Test
	public void add_shouldCallAdd() throws Exception {
		Result result = new Result(ResultStatus.SUCCESS, "code.message");
		
		Mockito.when(tvShowService.add(Mockito.anyString(), Mockito.any(Errors.class))).thenReturn(result);
		
		tvShowBridge.add("http://www.imdb.com/title/tt2575988/?ref_=nv_sr_1", "1");
		
		ArgumentCaptor<AsynchRequest> request = ArgumentCaptor.forClass(AsynchRequest.class);
		
		Mockito.verify(gateway, Mockito.times(1)).makeRequest(request.capture());
		
		RequestResult requestResult = request.getValue().execute();
		
		ArgumentCaptor<String> url = ArgumentCaptor.forClass(String.class);
		
		Mockito.verify(tvShowService, Mockito.times(1)).add(url.capture(), Mockito.any(Errors.class));
		Mockito.verifyNoMoreInteractions(tvShowService);
		
		Mockito.verifyZeroInteractions(messageConverter);
		
		Mockito.verifyNoMoreInteractions(gateway);
		
		Assert.assertEquals("http://www.imdb.com/title/tt2575988/?ref_=nv_sr_1", url.getValue());
		Assert.assertEquals("1", requestResult.getToken());
		Assert.assertEquals(result, requestResult.getResult());
		Assert.assertNotNull(requestResult.getErrors());
	}
	@Test
	public void update_shouldCallUpdate() throws Exception {
		Result result = new Result(ResultStatus.SUCCESS, "code.message");
		
		Mockito.when(tvShowService.update(true)).thenReturn(result);
		
		tvShowBridge.update(true, "1");
		
		ArgumentCaptor<AsynchRequest> request = ArgumentCaptor.forClass(AsynchRequest.class);
		
		Mockito.verify(gateway, Mockito.times(1)).makeRequest(request.capture());
		
		RequestResult requestResult = request.getValue().execute();
		
		Mockito.verify(tvShowService, Mockito.times(1)).update(true);
		Mockito.verifyNoMoreInteractions(tvShowService);
		
		Mockito.verifyZeroInteractions(messageConverter);
		
		Mockito.verifyNoMoreInteractions(gateway);
		
		Assert.assertEquals("1", requestResult.getToken());
		Assert.assertEquals(result, requestResult.getResult());
		Assert.assertNull(requestResult.getErrors());
	}
}
