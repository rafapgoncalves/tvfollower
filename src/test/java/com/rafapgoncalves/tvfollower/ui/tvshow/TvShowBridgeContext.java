package com.rafapgoncalves.tvfollower.ui.tvshow;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

import com.rafapgoncalves.tvfollower.tvshow.TvShowService;
import com.rafapgoncalves.tvfollower.ui.JavaGateway;
import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;

@Configuration
@ComponentScan(
		basePackages={"com.rafapgoncalves.tvfollower.ui.tvshow"},
		useDefaultFilters=false,
		includeFilters=@ComponentScan.Filter(value=Controller.class, type=FilterType.ANNOTATION))
class TvShowBridgeContext {
	@Bean
	public static TvShowService tvShowService() {
		return Mockito.mock(TvShowService.class);
	}
	@Bean
	public static JavaGateway javaGateway() {
		return Mockito.mock(JavaGateway.class);
	}
	@Bean
	public static MessageConverter messageConverter() {
		return Mockito.mock(MessageConverter.class);
	}
}
