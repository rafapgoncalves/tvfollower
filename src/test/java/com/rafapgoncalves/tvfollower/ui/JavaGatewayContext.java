package com.rafapgoncalves.tvfollower.ui;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rafapgoncalves.tvfollower.ui.converter.MessageConverter;
import com.rafapgoncalves.utils.javafx.JavaFXWrapper;

@Configuration
class JavaGatewayContext {
	@Bean
	public JavaFXWrapper javaFXWrapper() {
		return Mockito.mock(JavaFXWrapper.class);
	}
	@Bean
	public TesterThreadNameListener testerThreadNameListener() {
		return Mockito.mock(TesterThreadNameListener.class);
	}
	@Bean
	public JavaGateway javaGateway(JavaFXWrapper javaFXWrapper) {
		return new JavaGateway(javaFXWrapper);
	}
	@Bean
	public MessageConverter messageConverter() {
		return Mockito.mock(MessageConverter.class);
	}
}
