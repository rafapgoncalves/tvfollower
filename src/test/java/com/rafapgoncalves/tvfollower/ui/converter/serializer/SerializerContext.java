package com.rafapgoncalves.tvfollower.ui.converter.serializer;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Component;

import com.rafapgoncalves.tvfollower.i18n.I18nService;

@Configuration
@ComponentScan(
		basePackages={"com.rafapgoncalves.tvfollower.ui.converter.serializer"},
		useDefaultFilters=false,
		includeFilters=@ComponentScan.Filter(value=Component.class, type=FilterType.ANNOTATION))
class SerializerContext {
	@Bean
	public static I18nService i18nService() {
		return Mockito.mock(I18nService.class);
	}
}
