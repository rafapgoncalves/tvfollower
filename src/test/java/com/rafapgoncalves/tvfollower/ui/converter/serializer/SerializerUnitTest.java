package com.rafapgoncalves.tvfollower.ui.converter.serializer;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rafapgoncalves.tvfollower.AllUnitTestSuite;
import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.i18n.I18nService;
import com.rafapgoncalves.tvfollower.i18n.Language;
import com.rafapgoncalves.tvfollower.ui.RequestResult;
import com.rafapgoncalves.utils.Result;
import com.rafapgoncalves.utils.ResultStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllUnitTestSuite.Context.class, SerializerContext.class})
public class SerializerUnitTest {
	@Autowired
	private LanguageSerializer languageSerializer;
	@Autowired
	private TvShowSerializer tvShowSerializer;
	@Autowired
	private RequestResultSerializer requestResultSerializer;
	@Autowired
	private I18nService i18nService;
	
	@Before
	public void reset() {
		Mockito.reset(i18nService);
	}
	
	@Test
	public void languageToJson_shouldReturnJsonObjectAsString() throws Exception {
		Language language = new Language("English", new Locale("en", "US"));
		String expectedJson = "{\"tag\":\"en_US\",\"name\":\"English\"}";
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(languageSerializer.getMessageClass(), languageSerializer);
		Gson gson = gsonBuilder.create();
		
		String returnedJson = gson.toJson(language);
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verifyZeroInteractions(i18nService);
	}
	@Test
	public void tvShowSerializer_shouldReturnJsonObjectAsString() throws Exception {
		TvShow tvShow = new TvShow();
		tvShow.setId("tt3489184");
		tvShow.setName("Constantine");
		tvShow.getLastEpisodes().add(new Episode(1, 13));
		tvShow.setLastEpisodeWatched(new Episode(1, 12));
		
		String expectedJson = "{\"id\":\"tt3489184\",\"name\":\"Constantine\",\"watching\":false,\"ended\":false,\"lastEpisodeWatched\":{\"season\":1,\"number\":12},\"lastEpisode\":{\"season\":1,\"number\":13},\"outdatedEpisodeCount\":1,\"nextEpisodeToWatch\":{\"season\":1,\"number\":13}}";
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(tvShowSerializer.getMessageClass(), tvShowSerializer);
		Gson gson = gsonBuilder.create();
		
		String returnedJson = gson.toJson(tvShow);
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verifyZeroInteractions(i18nService);
	}
	@Test
	public void requestResultSerializerSuccess_shouldReturnJsonObjectAsString() throws Exception {
		TvShow tvShow = new TvShow();
		tvShow.setId("tt3489184");
		tvShow.setName("Constantine");
		tvShow.setLastEpisodeWatched(new Episode(1, 13));
		
		String expectedJson = "{\"token\":\"1\",\"status\":\"SUCCESS\",\"message\":\"result.success\"}";
		
		Errors errors = new BeanPropertyBindingResult(tvShow, "bean");
		Result result = new Result(ResultStatus.SUCCESS, "result.success", null);
		RequestResult requestResult = new RequestResult("1", result, errors);
		
		Mockito.doAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		}).when(i18nService).get(Mockito.anyString(), Mockito.any(Object[].class));
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(requestResultSerializer.getMessageClass(), requestResultSerializer);
		Gson gson = gsonBuilder.create();
		
		String returnedJson = gson.toJson(requestResult);
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verify(i18nService, Mockito.times(1)).get("result.success", (Object[]) null);
		Mockito.verifyNoMoreInteractions(i18nService);
	}
	@Test
	public void requestResultSerializerError_shouldReturnJsonObjectAsString() throws Exception {
		TvShow tvShow = new TvShow();
		tvShow.setId("tt3489184");
		tvShow.setName("Constantine");
		tvShow.setLastEpisodeWatched(new Episode(0, 0));
		
		String expectedJson = "{\"token\":\"1\",\"status\":\"ERROR\",\"message\":\"result.error\",\"errors\":[{\"message\":\"tvShow.error\"},{\"field\":\"lastEpisodeWatched.season\",\"message\":\"lastEpisodeWatched.season.error\"},{\"field\":\"lastEpisodeWatched.number\",\"message\":\"lastEpisodeWatched.number.error\"}]}";
		
		Errors errors = new BeanPropertyBindingResult(tvShow, "bean");
		Object[] args = new Object[] { 1 };
		errors.rejectValue("lastEpisodeWatched.season", "lastEpisodeWatched.season.error", args, null);
		errors.rejectValue("lastEpisodeWatched.number", "lastEpisodeWatched.number.error");
		errors.reject("tvShow.error");
		
		Result result = new Result(ResultStatus.ERROR, "result.error", args);
		RequestResult requestResult = new RequestResult("1", result, errors);
		
		Mockito.doAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0);
			}
		}).when(i18nService).get(Mockito.anyString(), Mockito.any(Object[].class));
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(requestResultSerializer.getMessageClass(), requestResultSerializer);
		Gson gson = gsonBuilder.create();
		
		String returnedJson = gson.toJson(requestResult);
		
		Assert.assertEquals(expectedJson, returnedJson);
		
		Mockito.verify(i18nService, Mockito.times(1)).get("result.error", args);
		Mockito.verify(i18nService, Mockito.times(1)).get("tvShow.error", (Object[]) null);
		Mockito.verify(i18nService, Mockito.times(1)).get("lastEpisodeWatched.season.error", args);
		Mockito.verify(i18nService, Mockito.times(1)).get("lastEpisodeWatched.number.error", (Object[]) null);
		Mockito.verifyNoMoreInteractions(i18nService);
	}
}
