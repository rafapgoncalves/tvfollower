package com.rafapgoncalves.tvfollower;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.rafapgoncalves.tvfollower.ui.converter.serializer.SerializerUnitTest;

@RunWith(Suite.class)
@SuiteClasses({SerializerUnitTest.class})
public class UIComponentsSuite {

}
