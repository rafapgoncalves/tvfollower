package com.rafapgoncalves.tvfollower;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.rafapgoncalves.tvfollower.tvshow.ServiceUnitTest;

@RunWith(Suite.class)
@SuiteClasses({ServiceUnitTest.class})
public class ServiceUnitTestSuite {
}
