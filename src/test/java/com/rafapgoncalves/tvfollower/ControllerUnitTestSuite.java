package com.rafapgoncalves.tvfollower;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.rafapgoncalves.tvfollower.ui.JavaGatewayUnitTest;
import com.rafapgoncalves.tvfollower.ui.i18n.I18nBridgeUnitTest;
import com.rafapgoncalves.tvfollower.ui.tvshow.TvShowBridgeUnitTest;

@RunWith(Suite.class)
@SuiteClasses({JavaGatewayUnitTest.class, I18nBridgeUnitTest.class, TvShowBridgeUnitTest.class})
public class ControllerUnitTestSuite {
}
