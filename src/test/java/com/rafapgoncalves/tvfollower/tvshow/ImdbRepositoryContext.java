package com.rafapgoncalves.tvfollower.tvshow;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

import com.rafapgoncalves.tvfollower.tvshow.data.TvShowImdbRepository;

@Configuration
@ComponentScan(
		basePackages={"com.rafapgoncalves.tvfollower.tvshow.data"},
		useDefaultFilters=false,
		includeFilters=@ComponentScan.Filter(value=TvShowImdbRepository.class, type=FilterType.ASSIGNABLE_TYPE))
class ImdbRepositoryContext {
	
}
