package com.rafapgoncalves.tvfollower.tvshow;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rafapgoncalves.tvfollower.AllUnitTestSuite;
import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.tvshow.data.TvShowLocalRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllUnitTestSuite.Context.class, LocalRepositoryContext.class})
public class LocalRepositoryUnitTest {
	private static String dbpath;
	
	@Autowired
	private TvShowLocalRepository tvShowLocalRepository;
	
	static {
		try {
			Properties prop = new Properties();
			
			prop.load(LocalRepositoryUnitTest.class.getResourceAsStream("/app.properties"));
			dbpath = new File(LocalRepositoryUnitTest.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getPath() + File.separator + prop.getProperty("app.data.dbname");
		}
		catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeClass
	public static void createDBFile() throws IOException {
		Assert.assertNotNull(dbpath);
		
		InputStream is = LocalRepositoryUnitTest.class.getResourceAsStream("/test-data.json");
		Files.copy(is, Paths.get(dbpath), StandardCopyOption.REPLACE_EXISTING);
		
		is.close();
	}
	@AfterClass
	public static void destroyDBFile() {
		File testdb = new File(dbpath);
		testdb.delete();
	}
	
	@Test
	public void loadDatabase_shouldLoadTvShowsProperly() throws Exception {
		Assert.assertNotNull(tvShowLocalRepository);
		Assert.assertEquals(tvShowLocalRepository.count(), 3);
		
		List<TvShow> tvShows;
		TvShow tvShow;
		
		tvShows = tvShowLocalRepository.list();
		
		// Arrow
		tvShow = tvShows.get(0);
		
		Assert.assertNotNull(tvShow);
		Assert.assertEquals("tt2193021", tvShow.getId());
		Assert.assertEquals("Arrow", tvShow.getName());
		
		Assert.assertEquals(4, tvShow.getLastEpisodes().size());
		
		int i = 1;
		for(Episode episode : tvShow.getLastEpisodes()) {
			Assert.assertEquals(i, episode.getSeason());
			Assert.assertEquals((i == 4) ? 21 : 23, episode.getNumber());
			i++;
		}
		
		Assert.assertEquals(4, tvShow.getLastEpisodeWatched().getSeason());
		Assert.assertEquals(21, tvShow.getLastEpisodeWatched().getNumber());
		
		// Flash
		tvShow = tvShows.get(2);
		
		Assert.assertNotNull(tvShow);
		Assert.assertEquals("tt3107288", tvShow.getId());
		Assert.assertEquals("The Flash", tvShow.getName());
		
		i = 1;
		for(Episode episode : tvShow.getLastEpisodes()) {
			Assert.assertEquals(i, episode.getSeason());
			Assert.assertEquals((i == 2) ? 21 : 23, episode.getNumber());
			i++;
		}
		
		Assert.assertEquals(2, tvShow.getLastEpisodeWatched().getSeason());
		Assert.assertEquals(21, tvShow.getLastEpisodeWatched().getNumber());
	}
	
	@Test
	public void writeDatabase_shouldOvewriteWithSameContent() throws Exception {
		TvShow tvShow;
		
		tvShow = tvShowLocalRepository.get("tt2193021");
		tvShowLocalRepository.save(tvShow);
		
		List<String> expected;
		expected = Files.readAllLines(Paths.get(LocalRepositoryUnitTest.class.getResource("/test-data.json").getPath()));
	    Assert.assertEquals(expected, Files.readAllLines(Paths.get(dbpath)));
	}
	
	@Test
	public void listById_shouldFindTvShow() throws Exception {
		TvShow tvShow;
		
		tvShow = tvShowLocalRepository.get("tt2193021");
		
		Assert.assertNotNull(tvShow);
		Assert.assertEquals("tt2193021", tvShow.getId());
	}
	
	@Test
	public void listById_shouldReturnNull() throws Exception {
		TvShow tvShow;
		
		tvShow = tvShowLocalRepository.get("tt2193022");
		
		Assert.assertNull(tvShow);
	}
	
	@Test
	public void cudOperations_shouldCreateUpdateAndDeleteTvShows() throws Exception {
		TvShow tvShow;
		
		tvShow = new TvShow();
		
		tvShow.setId("tt2575988");
		tvShow.setName("Silicon Valley");
		tvShow.getLastEpisodes().add(new Episode(1, 8));
		tvShow.getLastEpisodes().add(new Episode(2, 10));
		tvShow.getLastEpisodes().add(new Episode(3, 10));
		
		tvShow.setLastEpisodeWatched(new Episode(3, 8));
		
		Assert.assertEquals(3, tvShowLocalRepository.count());
		
		tvShowLocalRepository.save(tvShow);
		
		Assert.assertEquals(4, tvShowLocalRepository.count());
		tvShow = tvShowLocalRepository.get("tt2575988");
		Assert.assertNotNull(tvShow);
		
		tvShow.setLastEpisodeWatched(new Episode(3, 9));
		
		tvShowLocalRepository.save(tvShow);
		
		Assert.assertEquals(4, tvShowLocalRepository.count());
		tvShow = tvShowLocalRepository.get("tt2575988");
		Assert.assertNotNull(tvShow);
		Assert.assertEquals(9, tvShow.getLastEpisodeWatched().getNumber());
		
		tvShowLocalRepository.delete("tt2575988");
		
		Assert.assertEquals(3, tvShowLocalRepository.count());
		tvShow = tvShowLocalRepository.get("tt2575988");
		Assert.assertNull(tvShow);
	}
	
	@Test
	public void listOutdated_shouldListOnlyOutdatedTvShows() throws Exception {
		List<TvShow> tvShows;
		
		tvShows = tvShowLocalRepository.listOutdated();
		
		Assert.assertEquals(1, tvShows.size());
		Assert.assertEquals("tt2661044", tvShows.get(0).getId());
	}
	
	@Test
	public void objectReference_shouldReturnDifferentReferences() throws Exception {
		TvShow tvShowList, tvShowSelect;
		
		tvShowList = tvShowLocalRepository.list().get(0);
		tvShowSelect = tvShowLocalRepository.get(tvShowList.getId());
		
		Assert.assertFalse((tvShowList == tvShowSelect));
		
		tvShowList = tvShowLocalRepository.listOutdated().get(0);
		tvShowSelect = tvShowLocalRepository.get(tvShowList.getId());
		
		Assert.assertFalse((tvShowList == tvShowSelect));
		
		TvShow tvShowSave;
		
		tvShowSave = tvShowLocalRepository.get("tt2193021");
		tvShowLocalRepository.save(tvShowSave);
		tvShowSelect = tvShowLocalRepository.get("tt2193021");
		
		Assert.assertFalse((tvShowSave == tvShowSelect));
	}
	
	@Test(expected=NullPointerException.class)
	public void addNull_shouldThrowException() throws Exception {
		TvShow tvShow = null;
		
		tvShowLocalRepository.save(tvShow);
	}
}
