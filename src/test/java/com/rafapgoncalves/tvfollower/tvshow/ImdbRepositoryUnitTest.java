package com.rafapgoncalves.tvfollower.tvshow;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rafapgoncalves.tvfollower.AllUnitTestSuite;
import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.tvshow.data.TvShowImdbRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllUnitTestSuite.Context.class, ImdbRepositoryContext.class})
public class ImdbRepositoryUnitTest {
	
	@Autowired
	private TvShowImdbRepository imdbRepository;
	
	@Test
	public void getItem_shouldReturnTvShow() throws Exception {
		TvShow tvShow;
		
		tvShow = imdbRepository.get("tt0108778");
		
		Assert.assertNotNull(tvShow);
		Assert.assertEquals("tt0108778", tvShow.getId());
		Assert.assertEquals("Friends", tvShow.getName());
		
		Assert.assertEquals(10, tvShow.getLastEpisodes().size());
		
		Episode[] episodes = new Episode[tvShow.getLastEpisodes().size()];
		tvShow.getLastEpisodes().toArray(episodes);
		
		for(int i = 0; i < tvShow.getLastEpisodes().size(); i++) {
			Assert.assertEquals(i+1, episodes[i].getSeason());
			
			if(i == 2 || i == 5) {
				Assert.assertEquals(25, episodes[i].getNumber());
			}
			else if(i == 9) {
				Assert.assertEquals(18, episodes[i].getNumber());
			}
			else {
				Assert.assertEquals(24, episodes[i].getNumber());
			}
		}
	}
	
	@Test
	public void getNull_shouldReturnNull() throws Exception {
		TvShow tvShow;
		
		tvShow = imdbRepository.get("tt0");
		
		Assert.assertNull(tvShow);
	}
}
