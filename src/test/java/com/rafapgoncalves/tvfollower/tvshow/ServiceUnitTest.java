package com.rafapgoncalves.tvfollower.tvshow;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.rafapgoncalves.tvfollower.AllUnitTestSuite;
import com.rafapgoncalves.tvfollower.Episode;
import com.rafapgoncalves.tvfollower.TvShow;
import com.rafapgoncalves.tvfollower.tvshow.TvShowService;
import com.rafapgoncalves.tvfollower.tvshow.data.TvShowImdbRepository;
import com.rafapgoncalves.tvfollower.tvshow.data.TvShowLocalRepository;
import com.rafapgoncalves.utils.Result;
import com.rafapgoncalves.utils.ResultStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AllUnitTestSuite.Context.class, ServiceContext.class})
public class ServiceUnitTest {
	@Autowired
	private TvShowLocalRepository tvShowLocalRepository;
	@Autowired
	private TvShowImdbRepository tvShowImdbRepository;
	@Autowired
	private TvShowService tvShowService;
	
	@Before
	public void reset() {
		Mockito.reset(tvShowLocalRepository);
		Mockito.reset(tvShowImdbRepository);
	}
	
	@Test
	public void addNew_shouldCallRepositorySave() throws Exception {
		final TvShow tvShow;
		String tvShowUrl;
		Errors errors;
		
		tvShow = new TvShow();
		tvShow.setId("tt3489184");
		tvShow.setName("Constantine");
		tvShow.getLastEpisodes().add(new Episode(1, 13));
		
		Mockito.when(tvShowLocalRepository.get("tt3489184")).thenReturn(null);
		Mockito.when(tvShowImdbRepository.get("tt3489184")).thenReturn(tvShow);
		
		tvShowUrl = "http://www.imdb.com/title/tt3489184/?ref_=nv_sr_1";
		errors = new BeanPropertyBindingResult(tvShowUrl, "bean");
		
		Result result = tvShowService.add(tvShowUrl, errors);
		
		Assert.assertEquals(ResultStatus.SUCCESS, result.getStatus());
		Assert.assertFalse(errors.hasErrors());
		
		Mockito.verify(tvShowLocalRepository, Mockito.times(2)).get("tt3489184");
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).save(tvShow);
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verify(tvShowImdbRepository, Mockito.times(1)).get("tt3489184");
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
	}
	@Test
	public void addExisting_shouldNotCallRepositorySave() throws Exception {
		final TvShow tvShow;
		String tvShowUrl;
		Errors errors;
		
		tvShow = new TvShow();
		tvShow.setId("tt2575988");
		tvShow.setName("Silicon Valley");
		tvShow.getLastEpisodes().add(new Episode(1, 8));
		tvShow.getLastEpisodes().add(new Episode(2, 10));
		tvShow.getLastEpisodes().add(new Episode(3, 10));
		
		Mockito.when(tvShowLocalRepository.get("tt2575988")).thenReturn(tvShow);
		
		tvShowUrl = "http://www.imdb.com/title/tt2575988/?ref_=nv_sr_1";
		errors = new BeanPropertyBindingResult(tvShowUrl, "bean");
		
		Result result = tvShowService.add(tvShowUrl, errors);
		
		Assert.assertEquals(ResultStatus.WARNING, result.getStatus());
		Assert.assertFalse(errors.hasErrors());
		
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2575988");
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
	}
	@Test
	public void addInvalidUrl_shouldNotCallRepositorySave() throws Exception {
		String tvShowUrl;
		Errors errors;
		
		tvShowUrl = "http://www.opensubtitles.org/en/search";
		errors = new BeanPropertyBindingResult(tvShowUrl, "bean");
		
		Result result = tvShowService.add(tvShowUrl, errors);
		
		Assert.assertEquals(ResultStatus.ERROR, result.getStatus());
		Assert.assertTrue(errors.hasErrors());
		
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
	}
	@Test
	public void saveValidWatched_shouldCallRepositorySave() throws Exception {
		TvShow watched, tvShow;
		Errors errors;
		
		watched = new TvShow();
		watched.setId("tt2661044");
		watched.setName("The 100");
		watched.getLastEpisodes().add(new Episode(1, 13));
		watched.getLastEpisodes().add(new Episode(2, 16));
		watched.getLastEpisodes().add(new Episode(3, 15));
		
		watched.setLastEpisodeWatched(new Episode(3, 14));
		
		errors = new BeanPropertyBindingResult(watched, "bean");
		
		tvShow = new TvShow();
		tvShow.setId("tt2661044");
		tvShow.setLastEpisodeWatched(new Episode(3, 15));
		
		Mockito.when(tvShowLocalRepository.get("tt2661044")).thenReturn(watched);
		
		Result result = tvShowService.saveWatched(tvShow, errors);
		
		Assert.assertEquals(ResultStatus.SUCCESS, result.getStatus());
		Assert.assertFalse(errors.hasErrors());
		
		ArgumentCaptor<TvShow> tvShowSaved = ArgumentCaptor.forClass(TvShow.class);
		
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2661044");
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).save(tvShowSaved.capture());
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
		
		Assert.assertEquals("tt2661044", tvShowSaved.getValue().getId());
		Assert.assertEquals("The 100", tvShowSaved.getValue().getName());
		Assert.assertEquals(3, tvShowSaved.getValue().getLastEpisodes().size());
		
		Episode[] episodes = new Episode[3];
		tvShowSaved.getValue().getLastEpisodes().toArray(episodes);
		
		Assert.assertEquals(1, episodes[0].getSeason());
		Assert.assertEquals(13, episodes[0].getNumber());
		Assert.assertEquals(2, episodes[1].getSeason());
		Assert.assertEquals(16, episodes[1].getNumber());
		Assert.assertEquals(3, episodes[2].getSeason());
		Assert.assertEquals(15, episodes[2].getNumber());
		
		Assert.assertEquals(3, tvShowSaved.getValue().getLastEpisodeWatched().getSeason());
		Assert.assertEquals(15, tvShowSaved.getValue().getLastEpisodeWatched().getNumber());
	}
	@Test
	public void saveNullWatched_shouldCallRepositorySave() throws Exception {
		TvShow watched, tvShow;
		Errors errors;
		
		watched = new TvShow();
		watched.setId("tt2661044");
		watched.setName("The 100");
		watched.getLastEpisodes().add(new Episode(1, 13));
		watched.getLastEpisodes().add(new Episode(2, 16));
		watched.getLastEpisodes().add(new Episode(3, 15));
		
		watched.setLastEpisodeWatched(new Episode(3, 14));
		
		errors = new BeanPropertyBindingResult(watched, "bean");
		
		tvShow = new TvShow();
		tvShow.setId("tt2661044");
		tvShow.setLastEpisodeWatched(null);
		
		Mockito.when(tvShowLocalRepository.get("tt2661044")).thenReturn(watched);
		
		Result result = tvShowService.saveWatched(tvShow, errors);
		
		Assert.assertEquals(ResultStatus.SUCCESS, result.getStatus());
		Assert.assertFalse(errors.hasErrors());
		
		ArgumentCaptor<TvShow> tvShowSaved = ArgumentCaptor.forClass(TvShow.class);
		
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2661044");
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).save(tvShowSaved.capture());
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
		
		Assert.assertEquals("tt2661044", tvShowSaved.getValue().getId());
		Assert.assertEquals("The 100", tvShowSaved.getValue().getName());
		Assert.assertEquals(3, tvShowSaved.getValue().getLastEpisodes().size());
		
		Episode[] episodes = new Episode[3];
		tvShowSaved.getValue().getLastEpisodes().toArray(episodes);
		
		Assert.assertEquals(1, episodes[0].getSeason());
		Assert.assertEquals(13, episodes[0].getNumber());
		Assert.assertEquals(2, episodes[1].getSeason());
		Assert.assertEquals(16, episodes[1].getNumber());
		Assert.assertEquals(3, episodes[2].getSeason());
		Assert.assertEquals(15, episodes[2].getNumber());
		
		Assert.assertEquals(null, tvShowSaved.getValue().getLastEpisodeWatched());
	}
	@Test
	public void saveInvalidWatched_shouldNotCallRepositorySave() throws Exception {
		TvShow watched, tvShow;
		Errors errors;
		
		watched = new TvShow();
		watched.setId("tt2661044");
		watched.setName("The 100");
		watched.getLastEpisodes().add(new Episode(1, 13));
		watched.getLastEpisodes().add(new Episode(2, 16));
		watched.getLastEpisodes().add(new Episode(3, 15));
		
		watched.setLastEpisodeWatched(new Episode(3, 14));
		
		errors = new BeanPropertyBindingResult(watched, "bean");
		
		tvShow = new TvShow();
		tvShow.setId("tt2661044");
		tvShow.setLastEpisodeWatched(new Episode());
		
		Mockito.when(tvShowLocalRepository.get("tt2661044")).thenReturn(watched);
		
		Result result = tvShowService.saveWatched(tvShow, errors);
		
		Assert.assertEquals(ResultStatus.ERROR, result.getStatus());
		Assert.assertTrue(errors.hasErrors());
		Assert.assertTrue(errors.hasFieldErrors("lastEpisodeWatched.season"));
		Assert.assertTrue(errors.hasFieldErrors("lastEpisodeWatched.number"));
		
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2661044");
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
	}
	@Test
	public void updateAllGood_shouldCallRepositorySaveMultipleTimes() throws Exception {
		List<TvShow> tvShowsListed = new ArrayList<TvShow>();
		List<TvShow> tvShowsCrawled = new ArrayList<TvShow>();
		
		tvShowsListed.add(new TvShow());
		tvShowsListed.get(tvShowsListed.size() - 1).setId("tt2193021");
		tvShowsListed.get(tvShowsListed.size() - 1).setName("Arrow");
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(2, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(3, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(4, 21));
		tvShowsListed.get(tvShowsListed.size() - 1).setLastEpisodeWatched(new Episode(4, 21));
		
		tvShowsListed.add(new TvShow());
		tvShowsListed.get(tvShowsListed.size() - 1).setId("tt2661044");
		tvShowsListed.get(tvShowsListed.size() - 1).setName("The 100");
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(1, 13));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(2, 16));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(3, 15));
		tvShowsListed.get(tvShowsListed.size() - 1).setLastEpisodeWatched(new Episode(3, 14));
		
		tvShowsListed.add(new TvShow());
		tvShowsListed.get(tvShowsListed.size() - 1).setId("tt3107288");
		tvShowsListed.get(tvShowsListed.size() - 1).setName("The Flash");
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(2, 21));
		tvShowsListed.get(tvShowsListed.size() - 1).setLastEpisodeWatched(new Episode(2, 21));
		
		
		tvShowsCrawled.add(new TvShow());
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setId("tt2193021");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setName("Arrow");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(2, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(3, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(4, 23));
		
		tvShowsCrawled.add(new TvShow());
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setId("tt2661044");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setName("The 100");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(1, 13));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(2, 16));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(3, 16));
		
		tvShowsCrawled.add(new TvShow());
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setId("tt3107288");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setName("The Flash");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(2, 23));
		
		
		Mockito.when(tvShowLocalRepository.list()).thenReturn(tvShowsListed);
		Mockito.when(tvShowLocalRepository.get("tt2193021")).thenReturn(tvShowsListed.get(0));
		Mockito.when(tvShowLocalRepository.get("tt2661044")).thenReturn(tvShowsListed.get(1));
		Mockito.when(tvShowLocalRepository.get("tt3107288")).thenReturn(tvShowsListed.get(2));
		
		Mockito.when(tvShowImdbRepository.get("tt2193021")).thenReturn(tvShowsCrawled.get(0));
		Mockito.when(tvShowImdbRepository.get("tt2661044")).thenReturn(tvShowsCrawled.get(1));
		Mockito.when(tvShowImdbRepository.get("tt3107288")).thenReturn(tvShowsCrawled.get(2));
		
		Result result = tvShowService.update(true);
		
		Assert.assertEquals(ResultStatus.SUCCESS, result.getStatus());
		
		ArgumentCaptor<TvShow> tvShowsSaved = ArgumentCaptor.forClass(TvShow.class);
		
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).list();
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2193021");
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2661044");
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt3107288");
		Mockito.verify(tvShowLocalRepository, Mockito.times(3)).save(tvShowsSaved.capture());
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verify(tvShowImdbRepository, Mockito.times(1)).get("tt2193021");
		Mockito.verify(tvShowImdbRepository, Mockito.times(1)).get("tt2661044");
		Mockito.verify(tvShowImdbRepository, Mockito.times(1)).get("tt3107288");
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
		
		for(TvShow tvShowSaved : tvShowsSaved.getAllValues()) {
			Episode[] episodes = new Episode[tvShowSaved.getLastEpisodes().size()];
			tvShowSaved.getLastEpisodes().toArray(episodes);
			
			if("tt2193021".equals(tvShowSaved.getId())) {
				Assert.assertEquals("Arrow", tvShowSaved.getName());
				Assert.assertEquals(4, episodes.length);
				Assert.assertEquals(1, episodes[0].getSeason());
				Assert.assertEquals(23, episodes[0].getNumber());
				Assert.assertEquals(2, episodes[1].getSeason());
				Assert.assertEquals(23, episodes[1].getNumber());
				Assert.assertEquals(3, episodes[2].getSeason());
				Assert.assertEquals(23, episodes[2].getNumber());
				Assert.assertEquals(4, episodes[3].getSeason());
				Assert.assertEquals(23, episodes[3].getNumber());
				
				Assert.assertEquals(4, tvShowSaved.getLastEpisodeWatched().getSeason());
				Assert.assertEquals(21, tvShowSaved.getLastEpisodeWatched().getNumber());
			}
			else if("tt2661044".equals(tvShowSaved.getId())) {
				Assert.assertEquals("The 100", tvShowSaved.getName());
				Assert.assertEquals(3, episodes.length);
				Assert.assertEquals(1, episodes[0].getSeason());
				Assert.assertEquals(13, episodes[0].getNumber());
				Assert.assertEquals(2, episodes[1].getSeason());
				Assert.assertEquals(16, episodes[1].getNumber());
				Assert.assertEquals(3, episodes[2].getSeason());
				Assert.assertEquals(16, episodes[2].getNumber());
				
				Assert.assertEquals(3, tvShowSaved.getLastEpisodeWatched().getSeason());
				Assert.assertEquals(14, tvShowSaved.getLastEpisodeWatched().getNumber());
			}
			else if("tt3107288".equals(tvShowSaved.getId())) {
				Assert.assertEquals("The Flash", tvShowSaved.getName());
				Assert.assertEquals(2, episodes.length);
				Assert.assertEquals(1, episodes[0].getSeason());
				Assert.assertEquals(23, episodes[0].getNumber());
				Assert.assertEquals(2, episodes[1].getSeason());
				Assert.assertEquals(23, episodes[1].getNumber());
				
				Assert.assertEquals(2, tvShowSaved.getLastEpisodeWatched().getSeason());
				Assert.assertEquals(21, tvShowSaved.getLastEpisodeWatched().getNumber());
			}
		}
	}
	@Test
	public void updateTimeouted_shouldCallRepositorySaveMultipleTimes() throws Exception {
		List<TvShow> tvShowsListed = new ArrayList<TvShow>();
		List<TvShow> tvShowsCrawled = new ArrayList<TvShow>();
		
		tvShowsListed.add(new TvShow());
		tvShowsListed.get(tvShowsListed.size() - 1).setId("tt2193021");
		tvShowsListed.get(tvShowsListed.size() - 1).setName("Arrow");
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(2, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(3, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(4, 21));
		tvShowsListed.get(tvShowsListed.size() - 1).setLastEpisodeWatched(new Episode(4, 21));
		
		tvShowsListed.add(new TvShow());
		tvShowsListed.get(tvShowsListed.size() - 1).setId("tt2661044");
		tvShowsListed.get(tvShowsListed.size() - 1).setName("The 100");
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(1, 13));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(2, 16));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(3, 15));
		tvShowsListed.get(tvShowsListed.size() - 1).setLastEpisodeWatched(new Episode(3, 14));
		
		tvShowsListed.add(new TvShow());
		tvShowsListed.get(tvShowsListed.size() - 1).setId("tt3107288");
		tvShowsListed.get(tvShowsListed.size() - 1).setName("The Flash");
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsListed.get(tvShowsListed.size() - 1).getLastEpisodes().add(new Episode(2, 21));
		tvShowsListed.get(tvShowsListed.size() - 1).setLastEpisodeWatched(new Episode(2, 21));
		
		
		tvShowsCrawled.add(new TvShow());
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setId("tt2193021");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setName("Arrow");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(2, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(3, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(4, 23));
		
		tvShowsCrawled.add(new TvShow());
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setId("tt3107288");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).setName("The Flash");
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(1, 23));
		tvShowsCrawled.get(tvShowsCrawled.size() - 1).getLastEpisodes().add(new Episode(2, 23));
		
		
		Mockito.when(tvShowLocalRepository.list()).thenReturn(tvShowsListed);
		Mockito.when(tvShowLocalRepository.get("tt2193021")).thenReturn(tvShowsListed.get(0));
		Mockito.when(tvShowLocalRepository.get("tt2661044")).thenReturn(tvShowsListed.get(1));
		Mockito.when(tvShowLocalRepository.get("tt3107288")).thenReturn(tvShowsListed.get(2));
		
		Mockito.when(tvShowImdbRepository.get("tt2193021")).thenReturn(tvShowsCrawled.get(0));
		Mockito.when(tvShowImdbRepository.get("tt2661044")).thenReturn(null);
		Mockito.when(tvShowImdbRepository.get("tt3107288")).thenReturn(tvShowsCrawled.get(1));
		
		Result result = tvShowService.update(true);
		
		Assert.assertEquals(ResultStatus.WARNING, result.getStatus());
		
		ArgumentCaptor<TvShow> tvShowsSaved = ArgumentCaptor.forClass(TvShow.class);
		
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).list();
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2193021");
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt2661044");
		Mockito.verify(tvShowLocalRepository, Mockito.times(1)).get("tt3107288");
		Mockito.verify(tvShowLocalRepository, Mockito.times(2)).save(tvShowsSaved.capture());
		Mockito.verifyNoMoreInteractions(tvShowLocalRepository);
		
		Mockito.verify(tvShowImdbRepository, Mockito.times(1)).get("tt2193021");
		Mockito.verify(tvShowImdbRepository, Mockito.times(1)).get("tt2661044");
		Mockito.verify(tvShowImdbRepository, Mockito.times(1)).get("tt3107288");
		Mockito.verifyNoMoreInteractions(tvShowImdbRepository);
		
		for(TvShow tvShowSaved : tvShowsSaved.getAllValues()) {
			Episode[] episodes = new Episode[tvShowSaved.getLastEpisodes().size()];
			tvShowSaved.getLastEpisodes().toArray(episodes);
			
			if("tt2193021".equals(tvShowSaved.getId())) {
				Assert.assertEquals("Arrow", tvShowSaved.getName());
				Assert.assertEquals(4, episodes.length);
				Assert.assertEquals(1, episodes[0].getSeason());
				Assert.assertEquals(23, episodes[0].getNumber());
				Assert.assertEquals(2, episodes[1].getSeason());
				Assert.assertEquals(23, episodes[1].getNumber());
				Assert.assertEquals(3, episodes[2].getSeason());
				Assert.assertEquals(23, episodes[2].getNumber());
				Assert.assertEquals(4, episodes[3].getSeason());
				Assert.assertEquals(23, episodes[3].getNumber());
				
				Assert.assertEquals(4, tvShowSaved.getLastEpisodeWatched().getSeason());
				Assert.assertEquals(21, tvShowSaved.getLastEpisodeWatched().getNumber());
			}
			else if("tt3107288".equals(tvShowSaved.getId())) {
				Assert.assertEquals("The Flash", tvShowSaved.getName());
				Assert.assertEquals(2, episodes.length);
				Assert.assertEquals(1, episodes[0].getSeason());
				Assert.assertEquals(23, episodes[0].getNumber());
				Assert.assertEquals(2, episodes[1].getSeason());
				Assert.assertEquals(23, episodes[1].getNumber());
				
				Assert.assertEquals(2, tvShowSaved.getLastEpisodeWatched().getSeason());
				Assert.assertEquals(21, tvShowSaved.getLastEpisodeWatched().getNumber());
			}
		}
	}
}
