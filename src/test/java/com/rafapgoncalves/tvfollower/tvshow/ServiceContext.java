package com.rafapgoncalves.tvfollower.tvshow;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Service;

import com.rafapgoncalves.tvfollower.tvshow.data.TvShowImdbRepository;
import com.rafapgoncalves.tvfollower.tvshow.data.TvShowLocalRepository;

@Configuration
@ComponentScan(
		basePackages={"com.rafapgoncalves.tvfollower.tvshow"},
		useDefaultFilters=false,
		includeFilters={@ComponentScan.Filter(value=Service.class, type=FilterType.ANNOTATION)})
class ServiceContext {
	@Bean
	public static TvShowLocalRepository tvShowLocalRepository() {
		return Mockito.mock(TvShowLocalRepository.class);
	}
	@Bean
	public static TvShowImdbRepository tvShowImdbRepository() {
		return Mockito.mock(TvShowImdbRepository.class);
	}
}
