package com.rafapgoncalves.tvfollower;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.rafapgoncalves.tvfollower.tvshow.ImdbRepositoryUnitTest;
import com.rafapgoncalves.tvfollower.tvshow.LocalRepositoryUnitTest;

@RunWith(Suite.class)
@SuiteClasses({LocalRepositoryUnitTest.class, ImdbRepositoryUnitTest.class})
public class RepositoryUnitTestSuite {
}
