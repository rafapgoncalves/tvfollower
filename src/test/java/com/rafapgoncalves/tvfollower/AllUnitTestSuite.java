package com.rafapgoncalves.tvfollower;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@RunWith(Suite.class)
@SuiteClasses({RepositoryUnitTestSuite.class, ServiceUnitTestSuite.class, ControllerUnitTestSuite.class, UIComponentsSuite.class})
public class AllUnitTestSuite {
	
	@Configuration
	@ImportResource({"classpath:root-context.xml"})
	public static class Context {
	}
}
